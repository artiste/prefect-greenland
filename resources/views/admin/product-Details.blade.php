@extends(backpack_view('blank'))

@section('header')
    <section class="container-fluid d-print-none">
        <a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
        <h2  style= "text-align: center;">
		<span class="text-capitalize">Product Details</span>
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
          <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
          <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.bootstrap4.min.css">
        </h2>
    </section>
@endsection

@section('content')
    <!-- data table -->
	<div class="table-wrapper">
        <div class="card-body table-full-width table-responsive">
            <table id="ProductTable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
				<thead>
					<tr>
                        <th>Product Image</th>
						<th>Product Name</th>
						<th>Product Description</th>
						<th>Quantity</th>
						<th>price</th>
                        <th>In Stock</th>
					</tr>
				</thead> 
                <tbody>
                    @foreach($dataList as $pcdata)
                        <tr>
                            <td> <img src="{{URL::to($pcdata['product_image'])}}" style="height: 80px; width: 80px;"></td>
                            <td>{{$pcdata['name'] }}</td>
                            <td>{{$pcdata['description'] }}</td>
                            <td>{{$pcdata['quantity'] }}</td>
                            <td>{{$pcdata['price'] }}</td>
                            <td>{{$pcdata['in_stock'] }}</td>
                            
                        </tr>
                    @endforeach
                </tbody>
			</table>   
		</div>		
	</div>
@endsection
@push('after_scripts')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#ProductTable').DataTable({
                'pageLength': 100,
                "serverSide": false,
                "processing": true,
            });
        });
    </script>
@endpush