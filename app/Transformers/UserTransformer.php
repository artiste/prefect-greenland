<?php

namespace App\Transformers;

use App\Models\User;
use Flugg\Responder\Transformers\Transformer;

class UserTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\User $user
     * @return array
     */
    public function transform(User $user)
    {

        $imagesURL = null;
        if($user->getMedia('profile_image')->first()!=null){
            $imagesURL = $user->getMedia('profile_image')->first()->getUrl();
        }

        return [
            'id' => (int) $user->id,
            'name'=>$user->name,
            'username'=>$user->username,
            'email'=>$user->email,
            'contact_number'=>$user->contact_number,
            'address'=>$user->address,
            'role'=>$user->role,
            'car_plate'=>$user->car_plate,
            'driving_license'=>$user->driving_license,
            'app_token'=>$user->app_token,
            'image'=>$imagesURL,
        ];
    }
}
