<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> @lang('lang.dashboard')</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('customer') }}'><i class='nav-icon la la-person-booth'></i> Customers</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('orders') }}'><i class="nav-icon la la-money-check"></i>@lang('lang.Orders')</a></li>
{{--<li class='nav-item'><a class='nav-link' href='{{ route('driversTasks') }}'><i class="nav-icon la la-money-check"></i>@lang('lang.Drivers Tasks')</a></li>--}}

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon la la-list-alt'></i> Categories</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i class="nav-icon la la-product-hunt"></i>Products</a></li>


@if(backpack_user()->role !=App\Models\User::ROLE_DRIVER)
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('driver') }}'><i class="nav-icon la la-user"></i> @lang('lang.Drivers')</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('user') }}'><i class="nav-icon la la-user"></i>@lang('lang.Users')</a></li>
@endif

{{--<li class="nav-item nav-dropdown">--}}
{{--    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon fa fa-group"></i>Products</a>--}}
{{--    <ul class="nav-dropdown-items">--}}
{{--        <li class="nav-item"><a class="nav-link"  href="{{ route('prodcutList') }}"><i class="nav-icon fa fa-magnet"></i> <span>Product List</span></a></li>--}}
{{--        <li class="nav-item"><a class="nav-link"  href="{{ route('AddProduct') }}"><i class="nav-icon fa fa-user"></i> <span>Add Products</span></a></li>--}}
{{--    </ul>--}}
{{--</li>--}}

