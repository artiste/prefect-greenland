<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Category extends Model
{
    use CrudTrait;
    
    protected $fillable = [
        'category_name',
        'category_description',
        'status'
    ];


    public function category(){
        return $this->belongsTo(Product::class,'category_id');
    }
}
