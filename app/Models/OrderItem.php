<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;

    protected $fillable=[
        'id',
        'sale_id',
        'line_items_id',
        'product_id',
        'name',
        'variation_id',
        'subtotal',
        'total',
        'sku',
        'qty',
    ];

    public function sale(){
        return $this->belongsTo(Sale::class,'sale_id','id');
    }

    public function product(){
        return $this->hasOne(Product::class,'id','product_id');
    }
}
