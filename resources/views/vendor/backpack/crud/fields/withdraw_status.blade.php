<!-- radio -->
@php
    $optionValue = old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '';

    // if the class isn't overwritten, use 'radio'
    if (!isset($field['attributes']['class'])) {
        $field['attributes']['class'] = 'radio';
    }

    $field['wrapper']['data-init-function'] = $field['wrapper']['data-init-function'] ?? 'bpFieldInitRadioElement';
    $role = backpack_user()->role;
    $status = App\Models\Withdraw::STATUS;
    $status_text = App\Models\Withdraw::STATUS_TEXT;
    $selected_status = App\Models\Withdraw::SELECTED_STATUS;
    $user_role = 'App\Models\User';
@endphp

@include('crud::fields.inc.wrapper_start')

    <div>
        <label>{!! $field['label'] !!}</label>
        @include('crud::fields.inc.translatable_icon')
    </div>

        <select  @if(($role==$user_role::ROLE_MEMBER && ($optionValue == $status['processing']||$optionValue == $status['reject']|| $optionValue == $status['approve']))||($role!=$user_role::ROLE_MEMBER && $optionValue == $status['cancel'])) disabled @endif class="form-control" name="{{ $field['name'] }}" @include('crud::fields.inc.attributes')>
            <option value="">--status--</option>
            <option value="{{$status['pending']}}" @if($optionValue == $status['pending']) selected @endif @if($role==$user_role::ROLE_MEMBER) disabled @endif>{{$selected_status['pending']}}</option>
            <option value="{{$status['processing']}}" @if($optionValue == $status['processing']) selected @endif>{{$selected_status['processing']}}</option>
            <option value="{{$status['approve']}}" @if($optionValue == $status['approve']) selected @endif>@if($optionValue == $status['approve']) {{$selected_status['approve']}} @else {{$status_text['approve']}} @endif</option>
            <option value="{{$status['reject']}}" @if($optionValue == $status['reject']) selected @endif>@if($optionValue == $status['reject']) {{$selected_status['reject']}} @else {{$status_text['reject']}} @endif</option>
            @if(($role!=$user_role::ROLE_MEMBER && $optionValue == $status['cancel'])||$role==$user_role::ROLE_MEMBER)
                <option value="{{$status['cancel']}}" @if($optionValue == $status['cancel']) selected @endif>@if($optionValue == $status['cancel']) {{$selected_status['cancel']}} @else {{$status_text['cancel']}} @endif</option>
            @endif
        </select>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif

@include('crud::fields.inc.wrapper_end')

