<?php
//
//namespace App\Models;
//
//use Illuminate\Database\Eloquent\Factories\HasFactory;
//use Illuminate\Database\Eloquent\Model;
//
//class Order extends Model
//{
//    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
//    /*
//   |--------------------------------------------------------------------------
//   | GLOBAL VARIABLES
//   |--------------------------------------------------------------------------
//   */
//    protected $table = 'orders';
//    protected $guarded = ['id'];
//    protected $fillable = [
//        'user_id',
//        'assignee',
//        'amount',
//        'wordpress_order_id',
//        'order_key',
//        'status',
//        'currency',
//        'discount_total',
//        'discount_tax',
//        'shipping_total',
//        'shipping_tax',
//        'total',
//        'total_tax',
//        'prices_include_tax',
//        'wordpress_customer_id',
//        'customer_ip_address',
//        'customer_note',
//        'payment_method',
//        'transaction_id',
//        'date_paid',
//        'date_completed',
//        'deleted_at',
//        'delivery_date',
//    ];
//
//    // protected $hidden = [];
//    // protected $dates = [];
//
//    //order status
//    CONST STATUS_NEW = 'new';
//    CONST STATUS_ON_HOLD = 'on-hold';
//    CONST STATUS_PROCESSING = 'processing';
//    CONST STATUS_CURRENT_DELIVERY = 'In Delivery';
//    CONST STATUS_COMPLETED = 'completed';
//    CONST STATUS_SHIPPING = 'shipped';
//    CONST STATUS_REFUNDED = 'refunded';
//    CONST STAUS_CANCELED = 'canceled';
//    CONST STATUS = ['on-hold', 'processing', 'completed', 'shipped', 'refunded'];
//    /*
//    |--------------------------------------------------------------------------
//    | FUNCTIONS
//    |--------------------------------------------------------------------------
//    */
//
//    /*
//    |--------------------------------------------------------------------------
//    | RELATIONS
//    |--------------------------------------------------------------------------
//    */
//    //Line Items
//    public function lineItem()
//    {
//        return $this->hasMany(LineItem::class, 'order_id');
//    }
//
//    public function billing()
//    {
//        return $this->hasOne(Billing::class, 'order_id');
//    }
//
//    public function shipping()
//    {
//        return $this->hasOne(Shipping::class, 'order_id');
//    }
//
//    public function remark()
//    {
//        return $this->hasMany(OrderRemark::class, 'order_id');
//    }
//
//    //user order
//    public function user()
//    {
//        return $this->belongsTo(User::class, 'user_id');
//    }
//
//    public function driver()
//    {
//        return $this->belongsTo(User::class, 'assignee');
//    }
//
//    /*
//    |--------------------------------------------------------------------------
//    | SCOPES
//    |--------------------------------------------------------------------------
//    */
//
//    /*
//    |--------------------------------------------------------------------------
//    | ACCESORS
//    |--------------------------------------------------------------------------
//    */
//
//    /*
//    |--------------------------------------------------------------------------
//    | MUTATORS
//    |--------------------------------------------------------------------------
//    */
//}
