<?php

namespace App\Http\Controllers\Admin;

use App\Constant;
use App\Http\Requests\OrderRequest;
use App\Models\Order as Sales;
use App\Models\Sale;
use App\Models\Shipping;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Codexshaper\WooCommerce\Models\Order;
use Codexshaper\WooCommerce\Facades\WooCommerce;
use App\Mail\AppMailer;
use Carbon\Carbon;

/**
 * Class OrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OrderCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Sale::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/orders');
        CRUD::setEntityNameStrings('order', 'orders');

        $this->setupFiltering();
    }

    private function setupFiltering()
    {
        $this->crud->addFilter([
            'type' => 'select2',
            'name' => 'assignee',
            'label' => 'Driver',
        ],
            function() {
                return User::where('role','=',User::ROLE_DRIVER)->get()->pluck('name', 'id')->toArray();
            },
            function($value) {
                $this->crud->addClause('where', 'assignee', $value);
            });


        $this->crud->addFilter([
            'name' => 'status',
            'type' => 'dropdown',
            'label' => 'Status'
        ], [
            Sale::STATUS_NEW => 'New',
            Sale::STATUS_ON_HOLD => 'On Hold',
            Sale::STATUS_PROCESSING => 'Processing',
            Sale::STATUS_CURRENT_DELIVERY => 'In Delivery',
            Sale::STATUS_COMPLETED => 'Completed',
            Sale::STATUS_CANCELED => 'Canceled',
        ], function ($value) { // if the filter is active
             $this->crud->addClause('where', 'status', $value);
        });

        $this->crud->addFilter([
            'type' => 'date_range',
            'name' => 'delivery_date',
            'label' => 'Delivery Date'
        ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                if(isset($dates)){
                    $this->crud->addClause('where', 'delivery_date', '>=', $dates->from);
                    $this->crud->addClause('where', 'delivery_date', '<=', $dates->to . ' 23:59:59');
                }
            });

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name' => 'invoice_number',
            'type' => 'text',
            'label' => 'Order Number'
        ]);

        $this->crud->addColumn([
            'name' => 'customer_id',
            'type' => 'select',
            'label' => 'Customer',
        ]);

        $this->crud->addColumn([
            'name' => 'total',
            'type' => 'text',
            'label' => __('lang.Amount')
        ]);

        $this->crud->addColumn([
            'name' => 'status',
            'label' => __('lang.Status'),
            'type' => 'select_from_array',
            'options' => [
                Sale::STATUS_NEW => 'New',
                Sale::STATUS_ON_HOLD => 'On Hold',
                Sale::STATUS_PROCESSING => 'Processing',
                Sale::STATUS_CURRENT_DELIVERY => 'In Delivery',
                Sale::STATUS_COMPLETED => 'Completed',
                Sale::STATUS_CANCELED => 'Canceled'],
        ]);

        $this->crud->addColumn([
            'label' => "Driver",
            'type' => 'select',
            'name' => 'assignee',
            'entity' => 'driver',
            'model' => "App\Models\User",
            'attribute' => 'name'
        ]);

        $this->crud->addColumn([
            'label' => "Area",
            'type' => 'select',
            'name' => 'order_id',
            'entity' => 'shippingAddress',
            'model' => "App\Models\Shipping",
            'attribute' => 'area'
        ]);

        $this->crud->addColumn([
            'label' => 'Delivery Date',
            'type' => 'date',
            'name' => 'delivery_date'
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(OrderRequest::class);

        $this->crud->addField([
            'name' => 'invoice_number',
            'type' => 'invoice_number',
            'label' => "Order Number",
            'attributes' => [
                'readonly' => 'readonly'
            ]
        ]);

        $this->crud->addField([
            'name' => 'customer_id',
            'type' => 'select',
            'label' => 'Customer',
            'entity' => 'customer',
            'model' => 'App\Models\Customer',
            'attribute' => 'name',
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }), //  you can use this to filter the results show in the sele
        ]);

        $this->crud->addField([
            'name' => 'order_items',
            'type' => 'order_items',
            'label' => "Order Items",
        ]);


        $this->crud->addField([
            'name' => 'subtotal',
            'type' => 'order_subtotal',
            'label' => "Subtotal",
            'default' => '0.00',
            'prefix' => 'MYR',
            'attributes' => [
                'readonly' => 'readonly',
                'step' => 'any'
            ]
        ]);

        $this->crud->addField([
            'name' => 'address_1',
            'type' => 'text',
            'label' => 'Shipping Address line 1',
        ]);

        $this->crud->addField([
            'name' => 'address_2',
            'type' => 'text',
            'label' => 'Shipping Address line 2',
        ]);

        $this->crud->addField([
            'name' => 'city',
            'type' => 'text',
            'label' => 'City',
            'wrapper' => [
                'class' => 'form-group col-md-4'
            ]
        ]);


        $this->crud->addField([
            'name' => 'postcode',
            'type' => 'number',
            'label' => 'Postcode',
            'wrapper' => [
                'class' => 'form-group col-md-4'
            ]
        ]);

        $this->crud->addField([
            'name' => 'state',
            'type' => 'select_from_array',
            'label' => 'State',
            'wrapper' => [
                'class' => 'form-group col-md-4'
            ],
            'options' => [
                'selangor' => "Selangor",
                'kuala lumpur' => "Kuala Lumpur",
                'negeri sembilan' => "Negeri Sembilan",
            ]
        ]);

        $this->crud->addField([
            'name' => 'country',
            'type' => 'hidden',
            'label' => 'Country',
            'value' => 'MY',
        ]);
        $this->crud->addField([
            'name' => 'area',
            'type' => 'text',
            'label' => 'Area',
            'wrapper' => [
                'class' => 'form-group col-md-6'
            ],
        ]);


        $this->crud->addField([
            'name' => 'shipping_total',
            'type' => 'order_shippingfee',
            'label' => 'Shipping Fee',
            'prefix' => 'MYR',
            'wrapper' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => ["step" => "any"]
        ]);
        $this->crud->addField([
            'name' => 'total',
            'type' => 'order_total',
            'label' => "Total",
            'default' => '0.00',
            'prefix' => 'MYR',
            'attributes' => [
                'readonly' => 'readonly',
                'step' => 'any'
            ]
        ]);

        $this->crud->addField([
            'name' => 'assignee',
            'type' => 'select',
            'label' => 'Driver',
            'entity' => 'driver',
            'model' => 'App\Models\User',
            'attribute' => 'name',
            'options' => (function ($query) {
                return $query->where('role', User::ROLE_DRIVER)->orderBy('name', 'ASC')->get();
            }), //  you can use this to filter the results show in the sele
        ]);

        $this->crud->addField([
            'label' => 'Delivery Date',
            'type' => 'date',
            'name' => 'delivery_date'
        ]);

        $this->crud->addField([
            'name' => 'status',
            'type' => 'select_from_array',
            'label' => 'Status',
            'options' => [
                Sale::STATUS_NEW => 'New',
                Sale::STATUS_ON_HOLD => 'On Hold',
                Sale::STATUS_PROCESSING => 'Processing',
                Sale::STATUS_CURRENT_DELIVERY => 'In Delivery',
                Sale::STATUS_COMPLETED => 'Completed',
                Sale::STATUS_CANCELED => 'Canceled'],
        ]);

    }

    public function store(Request $request)
    {
        $this->crud->hasAccessOrFail('create');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();

        $item = $this->crud->create($this->crud->getStrippedSaveRequest());
        $this->saveRelationshipInformation($request, $item);
        if ($request['status'] != Sale::STATUS_CANCELED ||
            $request['status'] != Sale::STATUS_REFUNDED ||
            $request['status'] != Sale::STATUS_ON_HOLD) {
            $item->date_paid = now();
        }
        $item->save();
        $this->data['entry'] = $this->crud->entry = $item;
        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        // return $this->crud->performSaveAction($item->getKey());
        return redirect('admin/orders');
    }

    private function saveRelationshipInformation($request, $sale)
    {
        $sale->shippingAddress()->create([
            'customer_id' => $sale->customer_id,
            'address_1' => $request['address_1'],
            'address_2' => $request['address_2'],
            'city' => $request['city'],
            'state' => $request['state'],
            'postcode' => $request['postcode'],
            'country' => $request['country'],
        ]);

        $itemsList = json_decode($request['order_items'], true);
        foreach ($itemsList as $item) {
            $sale->items()->create([
                'product_id' => $item['id'],
                'name' => $item['name'],
                'subtotal' => $item['total_amount'],
                'total' => $item['total_amount'],
                'qty' => $item['total_quantity'],
            ]);
        }
    }


    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->addField([
            'name' => 'invoice_number',
            'type' => 'invoice_number',
            'label' => "Order Number",
            'attributes' => [
                'readonly' => 'readonly'
            ]
        ]);

        $this->crud->addField([
            'name' => 'customer_id',
            'type' => 'select',
            'label' => 'Customer',
            'entity' => 'customer',
            'model' => 'App\Models\Customer',
            'attribute' => 'name',
            'options' => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
            'attributes' => [
                'readonly' => 'readonly'
            ]
        ]);

        $this->crud->addField([
            'name' => 'order_items',
            'type' => 'order_items',
            'label' => "Order Items",
        ]);


        $this->crud->addField([
            'name' => 'subtotal',
            'type' => 'order_subtotal',
            'label' => "Subtotal",
            'default' => '0.00',
            'prefix' => 'MYR',
            'attributes' => [
                'readonly' => 'readonly',
                'step' => 'any'
            ],
        ]);

        $this->crud->addField([
            'name' => 'address_1',
            'type' => 'text',
            'label' => 'Shipping Address line 1',
        ]);

        $this->crud->addField([
            'name' => 'address_2',
            'type' => 'text',
            'label' => 'Shipping Address line 2',
        ]);

        $this->crud->addField([
            'name' => 'city',
            'type' => 'text',
            'label' => 'City',
            'wrapper' => [
                'class' => 'form-group col-md-4'
            ]
        ]);


        $this->crud->addField([
            'name' => 'postcode',
            'type' => 'number',
            'label' => 'Postcode',
            'wrapper' => [
                'class' => 'form-group col-md-4'
            ]
        ]);

        $this->crud->addField([
            'name' => 'state',
            'type' => 'select_from_array',
            'label' => 'State',
            'wrapper' => [
                'class' => 'form-group col-md-4'
            ],
            'options' => [
                'selangor' => "Selangor",
                'kuala lumpur' => "Kuala Lumpur",
                'negeri sembilan' => "Negeri Sembilan",
            ]
        ]);

        $this->crud->addField([
            'name' => 'country',
            'type' => 'hidden',
            'label' => 'Country',
            'value' => 'MY',
        ]);
        $this->crud->addField([
            'name' => 'area',
            'type' => 'text',
            'label' => 'Area',
            'wrapper' => [
                'class' => 'form-group col-md-6'
            ],
        ]);


        $this->crud->addField([
            'name' => 'shipping_total',
            'type' => 'order_shippingfee',
            'label' => 'Shipping Fee',
            'prefix' => 'MYR',
            'wrapper' => [
                'class' => 'form-group col-md-6'
            ],
            'attributes' => [
                "step" => "any",
                "readonly" => "readonly"
            ]
        ]);
        $this->crud->addField([
            'name' => 'total',
            'type' => 'order_total',
            'label' => "Total",
            'default' => '0.00',
            'prefix' => 'MYR',
            'attributes' => [
                'readonly' => 'readonly',
                'step' => 'any'
            ]
        ]);

        $this->crud->addField([
            'name' => 'assignee',
            'type' => 'select',
            'label' => 'Driver',
            'entity' => 'driver',
            'model' => 'App\Models\User',
            'attribute' => 'name',
            'options' => (function ($query) {
                return $query->where('role', User::ROLE_DRIVER)->orderBy('name', 'ASC')->get();
            })
        ]);

        $this->crud->addField([
            'label' => 'Delivery Date',
            'type' => 'date',
            'name' => 'delivery_date'
        ]);

        $this->crud->addField([
            'name' => 'status',
            'type' => 'select_from_array',
            'label' => 'Status',
            'options' => [
                Sale::STATUS_NEW => 'New',
                Sale::STATUS_ON_HOLD => 'On Hold',
                Sale::STATUS_PROCESSING => 'Processing',
                Sale::STATUS_CURRENT_DELIVERY => 'In Delivery',
                Sale::STATUS_COMPLETED => 'Completed',
                Sale::STATUS_CANCELED => 'Canceled'],
        ]);

        $this->crud->addField([
            'name' => "received_image",
            'type' => 'group_images',
            'label' => "Images",
            'wrapper' => [
                'class' => 'form-group col-md-12'
            ],
        ]);
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);

        $this->crud->addColumn([
            'name' => 'created_at',
            'type' => 'date',
            'label' => __('lang.Purchase Date')
        ]);
        $this->crud->addColumn([
            'name' => 'status',
            'label' => __('lang.Status'),
            'type' => 'select_from_array',
            'wrapper' => ['class' => 'form-group col-md-12'],
            'options' => [0 => 'new', 1 => 'on-hold', 2 => 'preparing', 3 => 'completed', 4 => 'shipping', 4 => 'refunded'],
        ]);
        $this->crud->addColumn([
            'name' => 'shipping_address',
            'type' => 'text',
            'label' => 'Shipping Address'
        ]);

        $this->crud->addColumn([
            'name' => 'contact_number',
            'type' => 'text',
            'label' => 'Number'
        ]);
        $this->crud->addColumn([
            'name' => 'remark',
            'type' => 'text',
            'label' => 'Description'
        ]);
    }

    //orders
    public function orders(Request $request)
    {
        if ($request->ajax()) {
            $startDateTime = Carbon::createFromFormat('Y-m-d', $request['date']);
            $startDateTime->startOfDay();
            $endDateTime = Carbon::createFromFormat('Y-m-d', $request['todate']);
            $endDateTime->endOfDay();

            $orders = Sales::where('assignee', NULL)->whereBetween('created_at', array($startDateTime, $endDateTime))->get();

            $result = collect();
            foreach ($orders as $order) {
                $data['checkbox'] = '<input type="checkbox">';
                $data['id'] = $order->id;
                $data['date'] = $order->created_at->format('d M Y h:i A');
                $data['status'] = $order->status;
                $data['amount'] = $order->total;
                $data['payment_method'] = $order->payment_method;
                $data['city'] = isset($order->shipping) ? $order->shipping->city : "";
                $data['note'] = $order->customer_note;
                $data['driver'] = ($order->assignee == NULL) ? '-' : $order->driver->name;
                $data['action'] =
                    '<a href="' . url('admin/order/' . $order->id . '/detail') . '" class="btn btn-sm btn-link"><i class="la la-eye"></i>' . trans('backpack::crud.preview') . '</a>
                 <a href="javascript:void(0)" onclick="deleteEntry(' . $order->id . ')" class="btn btn-sm btn-link" data-button-type="delete"><i class="la la-trash"></i>' . trans('backpack::crud.delete') . '</a>';
                $result->push($data);
            }
            return DataTables::of($result)
                ->rawColumns(['action', 'checkbox'])
                ->make();
        }
        return view('admin.orders');
    }

    //order details
    public function orderDetail()
    {
        return view('admin/order-detail');
    }

    //Delete order
    public function delete(Request $request)
    {
        $order = Sales::find($request['id']);
        if ($order) {
            $order->delete();
            return 1;
        } else {
            return 1;
        }
    }

    //assign driver for an order
    public function assignDriver(Request $request)
    {
        if (!empty($request['orders']) || $request['orders'] != NULL) {
            for ($i = 0; $i < count($request['orders']); $i++) {
                $order = Sales::find($request['orders'][$i]);
                if ($order) {
                    $order->assignee = $request['driver'];
                    $order->save();
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    //update an order status
    public function updateStatus(Request $request)
    {
        $sales = Sales::find($request['order_id']);
        $oldStatus = $sales->status;
        $wordpress_order_id = $sales->wordpress_order_id;
        $status = $request['status'];

        if ($oldStatus != $request['status']) {
            $sales->status = $request['status'];
            $sales->Save();
            $data = [
                'status' => $status,
            ];
            $order = Order::update($wordpress_order_id, $data);
            $this->emailOrder($sales, $request['status']);
        }
        \Alert::add('success', 'Order status is successfully updated!')->flash();
        return redirect()->back();
    }

    //Send email to customer on order update
    public function emailOrder($sales, $status)
    {
        $customer = $sales->user;
        $data = [
            'senderName' => 'PerfectGreenland', //sender name optional field
            'receiverName' => $customer->name, //receiver name optional field
            'from' => config('mail.mailers.smtp.username'),//sender address mandatory field
            'to' => 'ahmadh24.r@gmail.com',//receiver address mandatory field
            'subject' => 'Order Status Update',//email subject optional field
            // 'emailBody' => 'Sending Mail from Laravel.',//email body optional field can place in template as well
            'view' => 'emails.orderUpdate', //view mandatory field
            'cc' => [], //cc to multiple emails optional array array field
            'bcc' => [], //bcc to multiple emails optional field
            'emailType' => 'Template', //email type can be Basic, Template, Attachment, . Basic email Does not support html templates  it is faster than template and attachments type
            'order_id' => $sales->wordpress_order_id,
            'status' => $status,
        ];
        $email = new AppMailer($data);
        $message = $email->send();
    }

    //driver orders
    public function driverOrders(Request $request)
    {
        if ($request->ajax()) {
            $startDateTime = Carbon::createFromFormat('Y-m-d', $request['date']);
            $startDateTime->startOfDay();
            $endDateTime = Carbon::createFromFormat('Y-m-d', $request['todate']);
            $endDateTime->endOfDay();

            $orders = Sales::whereNotNull('assignee')->whereBetween('created_at', array($startDateTime, $endDateTime))->get();

            $result = collect();
            foreach ($orders as $order) {
                $data['checkbox'] = '<input type="checkbox">';
                $data['id'] = $order->id;
                $data['date'] = $order->created_at->format('d M Y h:i A');
                $data['status'] = $order->status;
                $data['amount'] = $order->total;
                $data['payment_method'] = $order->payment_method;
                $data['city'] = $order->shipping->city;
                $data['note'] = $order->customer_note;
                $data['driver'] = ($order->assignee == NULL) ? '-' : $order->driver->name;
                $data['action'] =
                    '<a href="' . url('admin/order/' . $order->id . '/detail') . '" class="btn btn-sm btn-link"><i class="la la-eye"></i>' . trans('backpack::crud.preview') . '</a>
                 <a href="javascript:void(0)" onclick="deleteEntry(' . $order->id . ')" class="btn btn-sm btn-link" data-button-type="delete"><i class="la la-trash"></i>' . trans('backpack::crud.delete') . '</a>';
                $result->push($data);
            }
            return DataTables::of($result)
                ->rawColumns(['action', 'checkbox'])
                ->make();
        }
        return view('admin.driver_tasks');
    }

}

