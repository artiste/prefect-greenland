<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Product;
use App\Models\categosry;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use function React\Promise\all;
use Carbon\Carbon;
use Illuminate\Support\Str;
use DB;
use Session;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{

    public function getProduct(Request $request)
    {
      
      if ($request->ajax()) {
        $fromDate = Carbon::createFromFormat('Y-m-d', $request['fromdate']);
        $toDate = Carbon::createFromFormat('Y-m-d', $request['todate']);
        $collection = $this->reports($request);
        return DataTables::of($collection)
        ->rawColumns(['details'])
        ->toJson();
    }   
    	return view('admin.all_product');
    }

    public function reports($request)
    { 
        $fromDate = Carbon::createFromFormat('Y-m-d', $request['fromdate']);
        $fromDate->startOfDay();
        $toDate = Carbon::createFromFormat('Y-m-d', $request['todate']);
        $toDate->endOfDay();
        
       // $backpackUser = backpack_user();
        $productLists = Product::whereBetween('created_at', array($fromDate, $toDate))->get();
      
        $collection = collect();
        foreach ($productLists as $product) {
            $data['id'] = $product->id;
            $data['category_id'] = $product->category->category_name;
            $data['name'] = $product->name;
            $data['description'] = $product->description;
            $data['quantity'] = $product->quantity;
            $data['price'] = $product->price;
            $data['in_stock'] = $product->in_stock;
            $data['details'] = '<a class="" href="' . route('prodcutDetails', ['id' => $product->id, 'startDate' => $fromDate->format('Y-m-d'), 'todate' => $toDate->format('Y-m-d')]) . '">View</a>|<a class="" href="' . route('delete_product', ['id' => $product->id]) . '">delete</a>|<a class="" href="' . route('edit_product', ['id' => $product->id]) . '">edit</a>';
            $collection->push($data);
        }
            return $collection;  
    
    } 
    public function prodcutDetails(Request $request)
    {
        $data = $this->productreportDetails($request);
        return view('admin.product-Details')
        ->with('dataList', $data);      
    }

    public function productreportDetails($request)
    {
        $fromDate = Carbon::createFromFormat('Y-m-d', $request['startDate']);
        $fromDate->startOfDay();
        $toDate = Carbon::createFromFormat('Y-m-d', $request['todate']);
        $toDate->endOfDay();

        $products = Product::where('id', $request['id'])->whereBetween('created_at', array($fromDate, $toDate))->get();
            $dataList = collect();
            foreach ($products as $pdata) {
              $data['id'] = $pdata->id;
            //   $data['category_id'] = $pdata->category->name;
              $data['name'] = $pdata->name;
              $data['product_image'] = $pdata->product_image;
              $data['description'] = $pdata->description;
              $data['quantity'] = $pdata->quantity;
              $data['price'] = $pdata->price;
              $data['in_stock'] = $pdata->in_stock;
                $dataList->push($data);
            }
        return $dataList;
    } 
    public function index()
    {
    	return view('admin.add_product');
    }
   public function save_product(Request $request)
   {
        $data=array();
        $data['name'] = $request->name;
        $data['category_id'] = $request->category;
        $data['description']=$request->description;
        $data['quantity'] = $request->quantity;
        $data['price']=$request->price;    
        $data['in_stock']=$request->in_stock;   
        $image=$request->file('product_image');
        if ($image) {
           $image_name = time();
           $ext=strtolower($image->getClientOriginalExtension());
           $image_full_name=$image_name.'.'.$ext;
           $upload_path='image/';
           $image_url=$upload_path.$image_full_name;
           $success=$image->move($upload_path,$image_full_name);
           if ($success) {
                $data['product_image']=$image_url;
                Product::create($data);
                Session::put('message','Product added successfully!!');
                return Redirect::to('admin/add-product');
            }
        }else{
            $data['product_image']='';
            Product::create($data);
            Session::put('message','product added successfully without image!!');
            return Redirect::to('admin/add-product');
        }
   
    }
   public function delete_product(Request $request)
   {
       $product = Product::find($request['id']);
       if($product){
           $product->delete();
           Session::get('message','Product Deleted successfully! ');
           return Redirect::to('admin/product-list'); 
       }else{
           return 0;
       }
   }

   public function edit_product(Request $request)
   {  
       $product = Product::find($request['id']);
        return view('admin.edit_product');
   }

    public function update_product(Request $request)
   {
        $product = Product::find($request['id']);
        // dd($request);
        $data=array();
        $data['name'] = $request->name;
        $data['category_id'] = $request->category;
        $data['description']=$request->description;
        $data['quantity'] = $request->quantity;
        $data['price']=$request->price;    
        $data['in_stock']=$request->in_stock;  
        $image = $request->file('product_image');
        $image=$request->file('product_image');
        if ($image) {
           $image_name = time();
           $ext=strtolower($image->getClientOriginalExtension());
           $image_full_name=$image_name.'.'.$ext;
           $upload_path='image/';
           $image_url=$upload_path.$image_full_name;
           $success=$image->move($upload_path,$image_full_name);
           if ($success) {
                $data['product_image']=$image_url;
                $product->update($data);
                Session::put('message','Product updated successfully!!');
                return Redirect::to('admin/edit-product/'.$request['id']); 
            }
        }else{
            $data['product_image']=$product->product_image;
            $product->update($data);
            Session::put('message','Product updated successfully!!');
            return Redirect::to('admin/edit-product/'.$request['id']); 
        }
    } 
}
