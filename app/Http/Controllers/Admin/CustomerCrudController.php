<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CustomerRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CustomerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CustomerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Customer::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/customer');
        CRUD::setEntityNameStrings('customer', 'customers');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
           "name"=>'name',
           'label'=>'Name',
           'type'=>'text'
        ]);

        $this->crud->addColumn([
            "name"=>'contact_number',
            'label'=>'Contact Number',
            'type'=>'text'
        ]);
        $this->crud->addColumn([
            "name"=>'address',
            'label'=>'Billing Address',
            'type'=>'text'
        ]);
        $this->crud->addColumn([
            "name"=>'email',
            'label'=>'Email Address',
            'type'=>'email'
        ]);

        $this->crud->addColumn([
            "name"=>'remark',
            'label'=>'Note',
            'type'=>'text'
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CustomerRequest::class);

        $this->crud->addField([
            'name'=>'name',
            'type'=>'text',
            'label'=>'Customer Name'
        ]);

        $this->crud->addField([
            'name'=>'contact_number',
            'type'=>'number',
            'label'=>'Contact Number'
        ]);

        $this->crud->addField([
           'name'=>'address',
           'type'=>'address_algolia',
            'label'=>'Billing Address'
        ]);

        $this->crud->addField([
            'name'=>'email',
            'type'=>'email',
            'label'=>'Email Address'
        ]);
        $this->crud->addField([
            'name'=>'remark',
            'type'=>'text',
            'label'=>'Note'
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
