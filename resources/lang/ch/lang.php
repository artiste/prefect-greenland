<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
     //Dasboard
     'dashboard' => 'Dashboard',
     'dashboard_title' => '欢迎来到完美格陵兰仪表盘',
     'dashboard_details' => 'Welcome To Perfect Greenland System',
     'dashboard_registered_user' => 'Registered users.',
     'dashboard_newlead' => 'Today new lead.', 
 
     'Reports' => 'Reports', 
     'Customers' => 'Customers', 
     'Sales' => 'Sales', 
     'Top Sales' => 'Top Sales', 
     'Orders' => 'Orders', 
     'Loading' => 'Loading', 
     
     //crud and pagination
     'actions'=>"actions",
     'preview'=>"Preview",
     'edit'=>"Edit",
 
     //sideBar
     'dashboard' => '仪表板',
     'Leads'=>"潜在客户",
     'Users'=>"用户数",
     'Drivers'=>"车手",
     'Orders'=>"命令",
 
     //table heading for user and driver
     'Name'=>"名称",
     'Email Address'=>"电子邮件地址",
     'contact_number'=>"联系电话",
     'Address'=>"地址",
     'Role'=>"角色",
     'Registered'=>"注册于",
     'Number'=>   "联系电话",
     'Password'=> "密码",
 
     //table heading for orders
  
     'Purchase Date'=>"购买日期",
     'Status'=>"状态",
     'Amount'=>"量",
     'Created At'=>"创建于",
     'Updated At'=>"更新于",
     'Contact Address'=>"联系地址",
     'contact number'=>"联系电话",
     'Payment Method'=>"付款方法",
     'product listing'=>"产品清单",
     'shipping method'=>"邮寄方式",
     'remark'=>"备注 ",

];
