@extends(backpack_view('blank'))

@section('header')
    <section class="container-fluid">
		<meta charset="utf-8">
		<title>Edit product</title>
		<meta name="description" content="add new product.">
		<meta name="author" content="Łukasz Holeczek">
		<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
		<!-- end: Meta -->
		
		<!-- start: Mobile Specific -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
    </section></br>
@endsection
@php
if(isset(request()->id)){
	$product = App\Models\Product::find(request()->id);
}else{
	$product = null;
}
$categories = App\Models\Category::all();
@endphp
@section('content')
<div class="container" style="background-color: white;">
	<section class="panel panel-default">
		<div class="panel-heading"> 
			<h3 class="panel-title">Edit Product</h3> 
		</div> 
		<p class="alert-success">
			<?php
			$message=Session::get('message');
			if($message)
			{
				echo $message;
				Session::put('message',null);

			}
           ?>
		</p>
		<div class="panel-body">
		@if($product)
			<form action="{{route('update_product')}}" method="POST">
                 {{ csrf_field() }}
				<div class="form-group">
					<label for="name" class="col-sm-3 control-label">Product Name</label>
					<div class="col-sm-9">
						<input type="hidden" class="form-control" name="id" id="id" value="{{$product->id}}">
						<input type="text" class="form-control" name="name" id="name" value="{{$product->name}}">
					</div>
				</div>
				<div class="form-group">
					<label for="name" class="col-sm-3 control-label">Product Description</label>
					<div class="col-sm-9">
						<textarea class="form-control"name="description" id="description">{{$product->description}}</textarea>		
					</div>
				</div> 
				<div class="form-group">
					<label for="about" class="col-sm-3 control-label">Quantity</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="quantity" id="quantity" value="{{$product->quantity}}">
					</div>
				</div> 
				<div class="form-group">
					<label for="qty" class="col-sm-3 control-label"required>price</label>
					<div class="col-sm-3">
						<input type="text" class="form-control" name="price" id="price" value="{{$product->price}}">
					</div>
				</div> 
				<div class="form-group">
					<label for="tech" class="col-sm-3 control-label">Category</label>
					<div class="col-sm-3">
						<select class="selectpicker"name="category" id="category" style="padding-right:75px;padding-bottom:5px;padding-top:4px;">
							<option value="">--Select--</option>
							@foreach($categories as $category){
								<option value="{{$category->id}}" @if($category->id == $product->category_id) selected @endif>{{$category->category_name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-sm-3">   
					<label class="control-label small" for="in_stok">In Stock:</label>
					<input type="text" class="form-control" name="in_stock" id="in_stock" value="{{$product->in_stock}}">
				</div>
				<div class="control-group">
				  <label class="control-label" for="product_image">Image </label>
				  <div class="controls">
					<input class="input-file uniform_on" name="product_image" id="product_image" type="file">
				  </div>
				  @if($product->product_image != null)
					<div class="controls" id="image">
						<img src="{{URL::to($product->product_image)}}" style="height: 80px; width: 80px;"></td>
					</div>
				  @endif
				</div>   <hr>	
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Update Product </button>
					<button type="reset" class="btn">Cancel</button>
				</div></br>
			</form>  
		@else
			<p> Product Not Found!</p>
		@endif
		</div>
	</section> 
</div>
@endsection
@push('after_scripts')

<script>
	$(document).ready(function(){
		$("input[type=file]").change(function(){
			$("#image").hide();
		});
	});
</script>
 @endpush