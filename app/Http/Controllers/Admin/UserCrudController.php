<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Hash;
use App\Models\User;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UserCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\User::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/user');
        CRUD::setEntityNameStrings('user', 'users');
        $this->crud->addClause('where', 'role', '!=', User::ROLE_DRIVER);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
         // CRUD::setFromDb();
         $this->crud->addColumn([
            'name' => 'name',
            'type' => 'text',
            'label' => __('lang.Name')
            ]);

        $this->crud->addColumn([
            'name' => 'email',
            'type' => 'email',
            'label' => __('lang.Email Address')
            ]);
        $this->crud->addColumn([
            'name' =>'contact_number',
            'type' => 'text',
            'label' => __('lang.contact_number')
            ]);
        $this->crud->addColumn([
            'name' =>'address',
            'type' => 'text',
            'label' => __('lang.Address')
            ]);
        $this->crud->addColumn([
            'name' => 'role',
            'label' => __('lang.Role'),
            'type' => 'select_from_array',
            'wrapper' => ['class' => 'form-group col-md-12'],
            'options' => [1 => 'Admin', 2 => 'Customer'],
            ]);
        // select2 filter
        $this->crud->addFilter([
            'name'  => 'role',
            'type'  => 'select2',
            'label' => __('Role')
        ], function () {
            return [
            1 => 'Admin',
            2 => 'Customer',
            ];
        },
            function ($value) { // if the filter is active
            $this->crud->addClause('where', 'role', $value);
        });
        CRUD::column('created_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(UserRequest::class);

        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'label' => __('lang.Name')
            ]);
    
        $this->crud->addField([
            'name' => 'email',
            'type' => 'email',
            'label' => __('lang.Email Address')
            ]);
        $this->crud->addField([
            'name' =>'contact_number',
            'type' => 'text',
            'label' => __('lang.Number')
                ]);
        $this->crud->addField([
            'name' =>'password',
            'type' => 'text',
            'label' => __('lang.Password')
                ]);
        //CRUD::field('password');
        CRUD::addField([
            'name' =>'address',
            'type' => 'text',
            'label' => __('lang.address')
                ]);
        $this->crud->addField([
            'name' => 'role',
            'label' => __('lang.Role'),
            'type' => 'select_from_array',
            'wrapper' => ['class' => 'form-group col-md-12'],
            'options' => [1 => 'Admin', 2 => 'Customer'],
        ]);
       // CRUD::field('created_at');
        //CRUD::field('wordpress_user_id');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        //$this->setupCreateOperation();
        $this->crud->addField([
        'name' => 'name',
        'type' => 'text',
        'label' => __('lang.Name')
        ]);

        $this->crud->addField([
            'name' => 'email',
            'type' => 'email',
            'label' => __('lang.Email Address')
            ]);
        $this->crud->addField([
            'name' =>'contact_number',
            'type' => 'text',
            'label' => __('lang.Number')
            ]);
        CRUD::Field('address');
        $this->crud->addField([
            'name' => 'role',
            'label' => __('lang.Role'),
            'type' => 'select_from_array',
            'wrapper' => ['class' => 'form-group col-md-12'],
            'options' => [1 => 'Admin', 2 => 'Customer'],
            ]);
        //CRUD::Field('created_at');
        //CRUD::Field('updated_at');
    }
    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);
        $this->crud->addColumn([
            'name' => 'name',
            'type' => 'text',
            'label' => __('lang.Name')
            ]);

        $this->crud->addColumn([
            'name' => 'email',
            'type' => 'email',
            'label' => __('lang.Email Address')
            ]);
        $this->crud->addColumn([
            'name' =>'contact_number',
            'type' => 'text',
            'label' => __('lang.contact_number')
            ]);
        $this->crud->addColumn([
            'name' =>'address',
            'type' => 'text',
            'label' => __('lang.Address')
            ]);
        $this->crud->addColumn([
            'name' => 'role',
            'label' => __('lang.Role'),
            'type' => 'select_from_array',
            'wrapper' => ['class' => 'form-group col-md-12'],
            'options' => [1 => 'Admin', 2 => 'Customer'],
            ]);

        $this->crud->removeButton( 'preview' );
        $this->crud->removeButton( 'update' );
        $this->crud->removeButton( 'revisions' );
        $this->crud->removeButton( 'delete' );
    }
    public function store(Request $request)
    {

        $this->crud->hasAccessOrFail('create');
        $request->offsetSet('password', Hash::make($request['password']));
        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();

        // insert item in the db
        $item = $this->crud->create($this->crud->getStrippedSaveRequest());
        $this->data['entry'] = $this->crud->entry = $item;

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }
}
