<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Codexshaper\WooCommerce\Facades\WooCommerce;
use Codexshaper\WooCommerce\Facades\Report;
use App\Models\Order;
use App\Models\User;

class ReportController extends Controller
{
    //get reports
    public function index(){
        $today = now()->format('Y-m-d');
        $query = [
            'date_min' => '2020-12-01', 
            'date_max' => $today
        ];
        $sales = Order::whereIn('status', ['processing', 'completed'])->sum('total');
        $topSeller = Report::topSellers($query);
        $customers = User::where('role', User::ROLE_CUSTOMER)->count('id');
        $orders = Order::count('id');
        $products = Report::products($query);
       
        // $totalSales = 0;
        // foreach($sales as $sale){
        //     $totalSales += $sale->total_sales;
        // }

        $topSales = collect();
        foreach($topSeller as $seller){
            $sellerData['name'] = $seller->name;
            $sellerData['quantity'] = $seller->quantity;
            $topSales->add($sellerData);
        }
        
        // $totalCustomers = collect();
        // $customersTotal = 0;
        // foreach($customers as $customer){
        //     $customerData['slug'] = $customer->slug;
        //     $customerData['total'] = $customer->total; 
        //     $customersTotal += $customer->total;   
        //     $totalCustomers->add($customerData);
        // }
        // $custTotal['total'] = $customersTotal;
        // $totalCustomers->add($custTotal);
        
        // $totalOrders = collect();
        // $ordersTotal = 0;
        // foreach($orders as $order){
        //     $orderData['slug'] = $order->slug;
        //     $orderData['total'] = $order->total; 
        //     $ordersTotal += $order->total;  
        //     $totalOrders->add($orderData);
        // }
        // $ordTotal['total'] = $ordersTotal;
        // $totalOrders->add($ordTotal);

        $totalProducts = collect();
        $ordersProduct = 0;
        foreach($products as $product){
            $orderData['slug'] = $product->slug;
            $orderData['total'] = $product->total; 
            $ordersProduct += $product->total;  
            $totalProducts->add($orderData);
        }
        $prodTotal['total'] = $ordersProduct;
        $totalProducts->add($prodTotal);

        $result = array(
            'totalSales'=>$sales, 
            'topSales'=>$topSales,
            'customers'=>$customers,
            'orders'=>$orders,
            'products'=>$totalProducts
        );
        
        return $result;    
    }
}
