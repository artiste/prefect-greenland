<div class="row">
    <div class="col-md-8">
        <span id="date-label-from" class="date-label">From Date:</span>
        <input class="date" type="date" id="selectedDate" required=""/>
        <span id="date-label-from" class="date-label">To Date:</span>
        <input class="date" type="date" id="selectDate" required=""/>
        <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
    </div>
@push('after_scripts')
<script>
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    $('#selectedDate').val(today);
    $('#selectDate').val(today);
</script>
@endpush