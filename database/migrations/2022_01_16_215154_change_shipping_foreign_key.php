<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeShippingForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipping_address', function (Blueprint $table) {
            $table->dropForeign(['user_id']);

            $table->renameColumn('user_id','customer_id');
            $table->foreign('customer_id')->on('customers')->references('id')->cascadeOnDelete()->cascadeOnUpdate();
            $table->string('area')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipping_address', function (Blueprint $table) {
            $table->dropColumn('customer_id');
            $table->dropColumn('area');
        });
    }
}
