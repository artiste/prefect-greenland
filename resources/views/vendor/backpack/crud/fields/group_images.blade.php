<!-- bootstrap datepicker input -->

<?php
$type = $field['name'];
$images = $entry->getImageURL();
?>


@include('crud::fields.inc.wrapper_start')
<label>{!! $field['label'] !!}</label>
<div class="container">
    <div class="row">
        @foreach($images as $media)
            <div class="col-md-4 col-xs-12">
                <img class="d-block w-100"
                     src="{{$media}}"
                     alt="First slide">
            </div>
        @endforeach
    </div>
</div>
@include('crud::fields.inc.wrapper_end')
