<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('line_items', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->bigInteger('order_id')->nullable();
            $table->smallInteger('quantity')->nullable();
            $table->string('tax_class')->nullable();
            $table->decimal('subtotal', 9, 2)->nullable();
            $table->decimal('subtotal_tax', 9, 2)->nullable();
            $table->decimal('total', 9, 2)->nullable();
            $table->decimal('total_tax', 9, 2)->nullable();
            $table->string('sku')->nullable();
            $table->decimal('price', 9, 2)->nullable();
            $table->bigInteger('wp_id')->nullable();
            $table->bigInteger('wp_order_id')->nullable();
            $table->bigInteger('wp_product_id')->nullable();
            $table->bigInteger('wp_variation_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('line_items');
    }
}
