<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Customer extends Model implements HasMedia
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasFactory;
    use SoftDeletes;
    use InteractsWithMedia;

    protected $fillable=[
        'id',
        'wordpress_id',
        'name',
        'address',
        'email',
        'contact_number',
        'created_at',
        'updated_at',
        'delected_at',
        'remark'
    ];


    public function orders(){
        return $this->hasMany(Sale::class,'customer_id','id');
    }

    public function shippingAddress(){
        return $this->hasMany(Shipping::class,'customer_id','id');
    }
}
