<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Product_image extends Model
{
    use CrudTrait;
    public $timestamp = false;
    protected $fillable = [
        'id',
        'product_id', 
        'image_path'
    ];

    public function image(){
        return $this->belongsTo(Product::class);
    }
}
