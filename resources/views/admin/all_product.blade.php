@extends(backpack_view('blank'))

@section('header')
    <section class="container-fluid d-print-none">
        <a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
        <h2  style= "text-align: center;">
		<span class="text-capitalize">All Product</span>
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
          <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.bootstrap4.min.css">
          <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.6/css/responsive.bootstrap4.min.css">
        </h2>
    </section>
@endsection

@section('content')
<div style="padding: 30px;">
        <div class="row">
            <div class="col-md-6">
                <span id="date-label-from" class="date-label">From Date:</span>
                <input class="date" type="date" id="selectedDate" required=""/>
                <span id="date-label-from" class="date-label">To Date:</span>
                <input class="date" type="date" id="selectDate" required=""/>
                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
            </div>
        </div>
    </div>
    <!-- data table -->
	<div class="table-wrapper">
        <div class="card-body table-full-width table-responsive">
            <table id="ProductTable" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
				<thead>
					<tr>
						<th>Category Name</th>
						<th>Product Name</th>
						<th>Product Description</th>
						<th>Quantity</th>
						<th>price</th>
						<th>In Stock</th>
						<th>Actions</th>
					</tr>
				</thead> 
                <tbody>
                </tbody>
			</table>   
		</div>		
	</div>
@endsection
@push('after_scripts')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function () {
            var now = new Date();
            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1)).slice(-2);
            var today = now.getFullYear() + "-" + (month) + "-" + (day);
            $('#selectedDate').val(today);
            $('#selectDate').val(today);

            load_data(today, today);
            function load_data(from_date = '', to_date = '') {
                $('#ProductTable').DataTable().destroy();
                $('#ProductTable').DataTable({
                    'pageLength': 10,
                    'searching': false,
                    "serverSide": false,
                    "processing": true,
                    "ajax": {
                        url: "{{ route('prodcutList') }}",
                        method: 'GET',
                        data: {
                            'fromdate': from_date,
                            'todate': to_date
                        }
                    },
                    "columns": [    
                        {data: 'category_id', name: 'category_id'},
                        {data: 'name', name: 'name'},
						{data: 'description', name: 'description'},
						{data: 'quantity', name: 'quantity'},
						{data: 'price', name: 'price'},
						{data: 'in_stock', name: 'in_stock'},
                        {data: 'details', name: 'details', orderable: false, searchable: false},
                    ],
                });
            }

            //filter date range
            $('#filter').click(function () {
                var from_date = $('#selectedDate').val();
                var to_date = $('#selectDate').val();
                if (from_date != '' && to_date != '') {
                    load_data(from_date, to_date);
                } else {
                    alert('Both Date is required');
                }
            });
    })
 </script>
 @endpush