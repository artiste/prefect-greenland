<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    //Dasboard
    'dashboard' => 'Papan Pemuka',
    'dashboard_title' => 'Selamat Datang ke Sistem Data Big Oceanus',
    'dashboard_details' => 'Selamat datang ke Oceanus Big Data System, sistem kami akan mengemas kini petunjuk baru setiap hari.',
    'dashboard_registered_user' => 'Pengguna berdaftar.',
    'dashboard_registered_user_hint' => 'lebih lagi sehingga tonggak seterusnya.',

    'users'=>"Pengguna",
    'name'=>"Nama",
    'contact_number'=>"Nombor telefon",
    'actions'=>"tindakan",
    'preview'=>"pratonton",
    'edit'=>"Edit",
  

];
