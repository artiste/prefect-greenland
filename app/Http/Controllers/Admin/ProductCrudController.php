<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OrderRequest;
use App\Models\Order as Sales;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Yajra\DataTables\Facades\DataTables;
use Codexshaper\WooCommerce\Models\Order;
use Codexshaper\WooCommerce\Facades\WooCommerce;
use App\Mail\AppMailer;
use Carbon\Carbon;

/**
 * Class OrderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Product::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/product');
        CRUD::setEntityNameStrings('product', 'products');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name'      => 'row_number',
            'type'      => 'row_number',
            'label'     => '#',
            'orderable' => false,
        ])->makeFirstColumn();

        $this->crud->addColumn([
            'label' => "Product Image",
            'name' => "product_image",
            'type' => 'image',
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1, // omit or set to 0 to allow any aspect ratio
            // 'disk'      => 's3_bucket', // in case you need to show images from a different disk
            // 'prefix'    => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);

        $this->crud->addColumn([
            'name' => 'category_id',
            'label' => "Category",
            'type' => 'select',
            'entity' => 'Category',
            'model' => "App\Models\Category", // related model
            'attribute' => 'category_name',
        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'label' => "Product Name",
            'type' => 'text'
        ]);

        $this->crud->addColumn([
            'name' => 'quantity',
            'label' => "Available Stock",
            'type' => 'number'
        ]);

        $this->crud->addColumn([
            'name' => 'price',
            'label' => "Price",
            'type' => 'number',
            'prefix' => 'MYR',
            'attributes' => ['step' => 'any']
        ]);

        $this->crud->addColumn([
            'name' => 'in_stock',
            'label' => 'Status',
            'type' => 'select_from_array',
            'options' => [Product::IN_STOCK => 'In Stock', Product::OUT_OF_STOCK => 'Out Of Stock'],
        ]);

        $this->crud->addColumn([
            'name' => 'created_at',
            'type' => 'date',
            'label' => "Created Date"
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(OrderRequest::class);
        $this->crud->addField([
            'label' => "Product Image",
            'name' => "product_image",
            'type' => 'image',
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1, // omit or set to 0 to allow any aspect ratio
            // 'disk'      => 's3_bucket', // in case you need to show images from a different disk
            // 'prefix'    => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);

        $this->crud->addField([
            'name' => 'category_id',
            'label' => "Category",
            'type' => 'select',
            'entity' => 'Category',
            'model' => "App\Models\Category", // related model
            'attribute' => 'category_name',
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => "Product Name",
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'quantity',
            'label' => "Available Stock",
            'type' => 'number'
        ]);

        $this->crud->addField([
            'name' => 'price',
            'label' => "Price",
            'type' => 'number',
            'prefix' => 'MYR',
            'attributes' => ['step' => 'any']
        ]);

        $this->crud->addField([
            'name' => 'in_stock',
            'label' => 'Status',
            'type' => 'select_from_array',
            'options' => [Product::IN_STOCK => 'In Stock', Product::OUT_OF_STOCK => 'Out Of Stock'],
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->setupListOperation();
    }

    //orders
    public function orders(Request $request)
    {
        if ($request->ajax()) {
            $startDateTime = Carbon::createFromFormat('Y-m-d', $request['date']);
            $startDateTime->startOfDay();
            $endDateTime = Carbon::createFromFormat('Y-m-d', $request['todate']);
            $endDateTime->endOfDay();

            $orders = Sales::where('assignee', NULL)->whereBetween('created_at', array($startDateTime, $endDateTime))->get();

            $result = collect();
            foreach ($orders as $order) {
                $data['checkbox'] = '<input type="checkbox">';
                $data['id'] = $order->id;
                $data['date'] = $order->created_at->format('d M Y h:i A');
                $data['status'] = $order->status;
                $data['amount'] = $order->total;
                $data['payment_method'] = $order->payment_method;
                $data['city'] = isset($order->shipping) ? $order->shipping->city : "";
                $data['note'] = $order->customer_note;
                $data['driver'] = ($order->assignee == NULL) ? '-' : $order->driver->name;
                $data['action'] =
                    '<a href="' . url('admin/order/' . $order->id . '/detail') . '" class="btn btn-sm btn-link"><i class="la la-eye"></i>' . trans('backpack::crud.preview') . '</a>
                 <a href="javascript:void(0)" onclick="deleteEntry(' . $order->id . ')" class="btn btn-sm btn-link" data-button-type="delete"><i class="la la-trash"></i>' . trans('backpack::crud.delete') . '</a>';
                $result->push($data);
            }
            return DataTables::of($result)
                ->rawColumns(['action', 'checkbox'])
                ->make();
        }
        return view('admin.orders');
    }

    //order details
    public function orderDetail()
    {
        return view('admin/order-detail');
    }

    //Delete order
    public function delete(Request $request)
    {
        $order = Sales::find($request['id']);
        if ($order) {
            $order->delete();
            return 1;
        } else {
            return 1;
        }
    }

    //assign driver for an order
    public function assignDriver(Request $request)
    {
        if (!empty($request['orders']) || $request['orders'] != NULL) {
            for ($i = 0; $i < count($request['orders']); $i++) {
                $order = Sales::find($request['orders'][$i]);
                if ($order) {
                    $order->assignee = $request['driver'];
                    $order->save();
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    //update an order status
    public function updateStatus(Request $request)
    {
        $sales = Sales::find($request['order_id']);
        $oldStatus = $sales->status;
        $wordpress_order_id = $sales->wordpress_order_id;
        $status = $request['status'];

        if ($oldStatus != $request['status']) {
            $sales->status = $request['status'];
            $sales->Save();
            $data = [
                'status' => $status,
            ];
            $order = Order::update($wordpress_order_id, $data);
            $this->emailOrder($sales, $request['status']);
        }
        \Alert::add('success', 'Order status is successfully updated!')->flash();
        return redirect()->back();
    }

    //Send email to customer on order update
    public function emailOrder($sales, $status)
    {
        $customer = $sales->user;
        $data = [
            'senderName' => 'PerfectGreenland', //sender name optional field
            'receiverName' => $customer->name, //receiver name optional field
            'from' => config('mail.mailers.smtp.username'),//sender address mandatory field
            'to' => 'ahmadh24.r@gmail.com',//receiver address mandatory field
            'subject' => 'Order Status Update',//email subject optional field
            // 'emailBody' => 'Sending Mail from Laravel.',//email body optional field can place in template as well
            'view' => 'emails.orderUpdate', //view mandatory field
            'cc' => [], //cc to multiple emails optional array array field
            'bcc' => [], //bcc to multiple emails optional field
            'emailType' => 'Template', //email type can be Basic, Template, Attachment, . Basic email Does not support html templates  it is faster than template and attachments type
            'order_id' => $sales->wordpress_order_id,
            'status' => $status,
        ];
        $email = new AppMailer($data);
        $message = $email->send();
    }

    //driver orders
    public function driverOrders(Request $request)
    {
        if ($request->ajax()) {
            $startDateTime = Carbon::createFromFormat('Y-m-d', $request['date']);
            $startDateTime->startOfDay();
            $endDateTime = Carbon::createFromFormat('Y-m-d', $request['todate']);
            $endDateTime->endOfDay();

            $orders = Sales::whereNotNull('assignee')->whereBetween('created_at', array($startDateTime, $endDateTime))->get();

            $result = collect();
            foreach ($orders as $order) {
                $data['checkbox'] = '<input type="checkbox">';
                $data['id'] = $order->id;
                $data['date'] = $order->created_at->format('d M Y h:i A');
                $data['status'] = $order->status;
                $data['amount'] = $order->total;
                $data['payment_method'] = $order->payment_method;
                $data['city'] = $order->shipping->city;
                $data['note'] = $order->customer_note;
                $data['driver'] = ($order->assignee == NULL) ? '-' : $order->driver->name;
                $data['action'] =
                    '<a href="' . url('admin/order/' . $order->id . '/detail') . '" class="btn btn-sm btn-link"><i class="la la-eye"></i>' . trans('backpack::crud.preview') . '</a>
                 <a href="javascript:void(0)" onclick="deleteEntry(' . $order->id . ')" class="btn btn-sm btn-link" data-button-type="delete"><i class="la la-trash"></i>' . trans('backpack::crud.delete') . '</a>';
                $result->push($data);
            }
            return DataTables::of($result)
                ->rawColumns(['action', 'checkbox'])
                ->make();
        }
        return view('admin.driver_tasks');
    }

}

