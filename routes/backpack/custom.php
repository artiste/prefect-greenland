<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes

    //routes for users
    Route::crud('user', 'UserCrudController');
    Route::get('user/{id}/chart', 'UserCrudController@usersgraph')->name('userChart');
    Route::get('Users-profile', 'ProfileController@userprofile')->name('userProfile');
    Route::crud('driver', 'DriverCrudController');
    //Route::get('order', 'OrderController@getOrder')->name('getorder');

     //routes for Orders
    Route::crud('orders', 'OrderCrudController');
//    Route::get('orders', 'OrderCrudController@orders')->name('orders');
//    Route::delete('orders/delete', 'OrderCrudController@delete')->name('deleteOrder');
//    Route::get('order/{id}/detail', 'OrderCrudController@orderDetail')->name('orderDetail');
    Route::post('store-new-assignee','OrderCrudController@assignDriver')->name('assignDriver');
    Route::post('update-status','OrderCrudController@updateStatus')->name('updateStatus');

    //driver tasks
    Route::get('drivers/tasks', 'OrderCrudController@driverOrders')->name('driversTasks');

    //Sync users
    Route::get('sync-users', 'SyncUserController@index');
    Route::post('syncUsers', 'SyncUserController@syncUsers')->name('syncUsers');

    //reports
    Route::get('reports', 'ReportController@index')->name('reports');
    //category routes
    Route::crud('category', 'CategoryCrudController');
    //products routes are here
    Route::crud('product', 'ProductCrudController');
    Route::get('add-product','ProductController@index')->name('AddProduct');
    Route::post('save-product','ProductController@save_product')->name('SaveProduct');
    Route::get('product-list','ProductController@getProduct')->name('prodcutList');
    Route::get('product-details','ProductController@prodcutDetails')->name('prodcutDetails');
    Route::get('delete-product/{id}','ProductController@delete_product')->name('delete_product');
    Route::get('edit-product/{id}','ProductController@edit_product')->name('edit_product');
    Route::post('update-product','ProductController@update_product')->name('update_product');
    Route::crud('customer', 'CustomerCrudController');
}); // this should be the absolute last line of this file