@extends(backpack_view('blank'))
@section('header')
    <section class="container-fluid">
        <h2 style="align-content: center; text-align: center">
            <span class="text-capitalize">Orders</span>
        </h2>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .hiddenColumn{
            display:none;
            }
        </style>
    </section>
@endsection
@section('content')
    <div class="col-md-12">
        <div class="table-wrapper">
            <div class="card-body table-full-width table-responsive">

                <!-- Filter table -->
            @include('layout.dateFilter')
            <!-- end filter -->
            </div>
            <br>
            <table id="ordersTable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th width="3%">#</th>
                    <th width="15%">Date</th>
                    <th width="8%">Status</th>
                    <th width="9%">Amount</th>
                    <th width="8%">Payment<br>Method</th>
{{--                    <th width="10%">City</th>--}}
                    <th width="22%">Remark</th>
                    <th width="10%">Driver</th>
                    <th width="15%">Action</th>
                    <th style="display:none;">ID</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="col-md-6" style= "padding: 15px;">
            @php
                $users = App\Models\User::where('role', App\Models\User::ROLE_DRIVER )->get();
            @endphp
            <form class="form-inline">
                <div class="form-group">
                    <label for="assignee">Driver &nbsp;</label>
                    <select class="custom-select my-1" id="assignee" name="assignee" required>
                        <option value="">--Select Driver--</option>
                        @foreach($users as $driver)
                            <option value="{{$driver->id}}">{{$driver->name}}</option>
                        @endforeach
                    </select>
                    &nbsp; &nbsp;
                    <button type="button" value="Assign" class="btn btn-primary" onclick="assignDriver()">Assign</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('after_scripts')
<script>
    var from = $('#selectedDate').val();
    var to = $('#selectDate').val();
    load_data(from, to);

    function load_data(from_date = '', to_date = '') {
        $('#ordersTable').DataTable().destroy();
        $("#ordersTable").DataTable({
            "serverSide": true,
            "processing": true,
            "responsive": true,
            "searching": false,
            'statesave': true,
            "ajax": {
                url: "{{ route('orders') }}",
                method: 'GET',
                data: {
                    'date': from_date,
                    'todate': to_date
                }
            },
            "pageLength": 10,
            "columns": [
                {data: 'checkbox', name: 'checkbox'},
                {data: 'date', name: 'date'},
                {data: 'status', name: 'status'},
                {data: 'amount', name: 'amount'},
                {data: 'payment_method', name: 'payment_method'},
                // {data: 'city', name: 'city'},
                {data: 'note', name: 'note'},
                {data: 'driver', name: 'driver'},
                {data: 'action', name: 'action'},
                {data: 'id', name: 'id', className: 'hiddenColumn'},
            ],
        });
    }

    //filter date range
    $('#filter').click(function () {
        var from_date = $('#selectedDate').val();
        var to_date = $('#selectDate').val();
        if (from_date != '' && to_date != '') {
            load_data(from_date, to_date);
        } else {
            alert('Both Date is required');
        }
    });

	function deleteEntry(id){
		swal({
		  title: "{!! trans('backpack::base.warning') !!}",
		  text: "{!! trans('backpack::crud.delete_confirm') !!}",
		  icon: "warning",
		  buttons: {
		  	cancel: {
			  text: "{!! trans('backpack::crud.cancel') !!}",
			  value: null,
			  visible: true,
			  className: "bg-secondary",
			  closeModal: true,
			},
		  	delete: {
			  text: "{!! trans('backpack::crud.delete') !!}",
			  value: true,
			  visible: true,
			  className: "bg-danger",
			}
		  },
		}).then((value) => {
			if (value) {
				$.ajax({
			      url: "{{route('deleteOrder')}}",
			      type: 'DELETE',
                  data: {'id': id},
			      success: function(result) {
			          if (result == 1) {
			          	  // Show a success notification bubble
			              new Noty({
		                    type: "success",
		                    text: "{!! '<strong>'.trans('backpack::crud.delete_confirmation_title').'</strong><br>'.trans('backpack::crud.delete_confirmation_message') !!}"
		                  }).show();

			              // Hide the modal, if any
			              $('.modal').modal('hide');
                          var from_date = $('#selectedDate').val();
                          var to_date = $('#selectDate').val();
                          load_data(from_date, to_date);
			          } else {
                        swal({
                        title: "{!! trans('backpack::crud.delete_confirmation_not_title') !!}",
                        text: "{!! trans('backpack::crud.delete_confirmation_not_message') !!}",
                        icon: "error",
                        timer: 4000,
                        buttons: false,
                        });
			          }
			      },
			      error: function(result) {
			          // Show an alert with the result
			          swal({
		              	title: "{!! trans('backpack::crud.delete_confirmation_not_title') !!}",
                        text: "{!! trans('backpack::crud.delete_confirmation_not_message') !!}",
		              	icon: "error",
		              	timer: 4000,
		              	buttons: false,
		              });
			      }
			    });
			}
		});
	}

    //Assign driver
    function assignDriver(){
        var driver = $("#assignee").val();
        var tableData= document.getElementById('ordersTable');
        orders=[];
        $('input:checkbox:checked', tableData).each(function() {
            orders.push($(this).closest('tr').find('td:nth-child(9)').text());
        }).get();
        console.log(orders.length);
        var process = false;
        if(orders.length <= 0 || orders.length==''|| orders.length== null){
            process = false
            alert('Please select an order');
        }else{
            process = true;
        }
        if(driver == null || driver==''){
            process = false
            alert('Please select a driver');
        }else{
            process = true;
        }
        if(process == true){
            $.ajax({
                url: "{{route('assignDriver')}}",
                type: 'POST',
                data: {'driver': driver, 'orders':orders},
                success: function(result) {
                    if (result == 1) {
                        // Show a success notification bubble
                        new Noty({
                        type: "success",
                        text: "{!! '<strong>Oder Assigned</strong><br> The order is successfully assigned to driver' !!}"
                        }).show();

                        // Hide the modal, if any
                        $('.modal').modal('hide');
                        var from_date = $('#selectedDate').val();
                        var to_date = $('#selectDate').val();
                        load_data(from_date, to_date);
                    } else {
                        swal({
                        title: "{!! trans('backpack::crud.delete_confirmation_not_title') !!}",
                        text: "{!! trans('backpack::crud.delete_confirmation_not_message') !!}",
                        icon: "error",
                        timer: 4000,
                        buttons: false,
                        });
                    }
                },
                error: function(result) {
                    // Show an alert with the result
                    swal({
                    title: "Error Occured",
                    text: "The order is not assigned to driver",
                    icon: "error",
                    timer: 4000,
                    buttons: false,
                    });
                }
            });
        }
    }
</script>
@endpush
