<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Codexshaper\WooCommerce\Models\Customer;
use Illuminate\Database\Seeder;

class SyncWordpressUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $customerList = Customer::all();
        // $password = bcrypt('secret');
        // foreach ($customerList as $customer){
        //     $createDate = Carbon::createFromFormat('Y-m-d\TH:i:s',$customer->date_created);
        //     User::create([
        //         'name' => $customer->first_name,
        //         'username' =>$customer->username,
        //         'password'=>$password,
        //         'email'=>$customer->email,
        //         'role'=>User::ROLE_MEMBER,
        //         'status'=>0,
        //         'sales'=>0,
        //         'commission'=>0,
        //         'email_verified_at'=>now(),
        //         'wordpress_user_id'=>$customer->id,
        //         'created_at'=>$createDate,
        //         'updated_at'=>$createDate,
        //     ]);
        // }
        $roles = [
            'customer', 
            'administrator', 
            'editor',
            'author',
            'contributor',
            'shop_manager',
            'subscriber',
            'colibri_content_editor',//content_editor
            'wc_product_vendors_admin_vendor',// 'vendor_admin'
            'wc_product_vendors_manager_vendor',// 'vendor_manager'
            'wc_product_vendors_pending_vendor',// 'pending_vendor',
            'seller',// 'vendor',
            'wcfm_vendor',
            'disable_vendor',
            'shop_staff',
            'wcfm_delivery_boy',// 'delivery_person',
            'wcfm_affiliate',// 'store_affiliates'
            'register_clerk',
            'outlet_manager',
            'woo_vou_vendors'// 'voucher_vendor'
        ];
        $users = collect();
        foreach($roles as $role){
            $user = Customer::where('role', $role)->get();
            $users =  $users->merge($user);
        }
        
        foreach ($users as $user){
            $existingUser = User::where('wordpress_user_id', $user->id)->first();
            if(!$existingUser){
                if($user->role == 'administrator'){
                    User::create([
                        'name'=>$user->first_name,
                        'username'=>$user->username,
                        'password'=>'',
                        'role'=>1,
                        'email'=>$user->email,
                        'created_at'=>Carbon::createFromFormat('Y-m-d\TH:i:s',$user->date_created),
                        'wordpress_user_id'=>$user->id
                    ]);
                }else{
                    User::create([
                        'name'=>$user->first_name,
                        'username'=>$user->username,
                        'password'=>'',
                        'role'=>2,
                        'email'=>$user->email,
                        'created_at'=>Carbon::createFromFormat('Y-m-d\TH:i:s',$user->date_created),
                        'wordpress_user_id'=>$user->id
                    ]);
                }
            }
        }
    }
}
