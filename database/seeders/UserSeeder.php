<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i<4;$i++){
            DB::table('users')->insert([
                'name' => Str::random(10),
                'username' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'contact_number'=>'01'.rand(10000000,99999999),
                'address'=>Str::random(10),
                'role'=>1,
                'password' => Hash::make('secret'),
            ]);
        }

        $user1 = User::where('id', '!=', 5)->orderBy('id', 'desc')->limit(3)->get();
        $firstuser = User::orderBy('id', 'asc')->first();
        foreach ($user1 as $user){
            $user->parrent_id = $firstuser->id;
            $user->save();
            DB::table('user_children')->insert([
                'user_id' => $firstuser->id,
                'child_id' => $user->id,
                'level' => '1',
            ]);
        }
        for($i=0; $i<3;$i++){
            DB::table('users')->insert([
                'name' => Str::random(10),
                'username' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'contact_number'=>'01'.rand(10000000,99999999),
                'address'=>Str::random(10),
                'role'=>1,
                'password' => Hash::make('secret'),
            ]);
        }
        $user2 = User::where('id', '!=', 5)->orderBy('id', 'desc')->limit(3)->get();
        foreach ($user2 as $user){
            $user->parrent_id = $firstuser->id;
            $user->save();
            DB::table('user_children')->insert([
                'user_id' => $firstuser->id,
                'child_id' => $user->id,
                'level' => '2',
            ]);
        }
        for($i=0; $i<3;$i++){
            DB::table('users')->insert([
                'name' => Str::random(10),
                'username' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'contact_number'=>'01'.rand(10000000,99999999),
                'address'=>Str::random(10),
                'role'=>1,
                'password' => Hash::make('secret'),
            ]);
        }
        $user3 = User::where('id', '!=', 5)->orderBy('id', 'desc')->limit(3)->get();
        foreach ($user3 as $user){
            $user->parrent_id = $firstuser->id;
            $user->save();
            DB::table('user_children')->insert([
                'user_id' => $firstuser->id,
                'child_id' => $user->id,
                'level' => '3',
            ]);
        }     
        for($i=0; $i<3;$i++){
            DB::table('users')->insert([
                'name' => Str::random(10),
                'username' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'contact_number'=>'01'.rand(10000000,99999999),
                'address'=>Str::random(10),
                'role'=>1,
                'password' => Hash::make('secret'),
            ]);
        }
        $user4 = User::where('id', '!=', 5)->orderBy('id', 'desc')->limit(3)->get();
        foreach ($user4 as $user){
            $user->parrent_id = $firstuser->id;
            $user->save();
            DB::table('user_children')->insert([
                'user_id' => $firstuser->id,
                'child_id' => $user->id,
                'level' => '4',
            ]);
        }  
        
        for($i=0; $i<3;$i++){
            DB::table('users')->insert([
                'name' => Str::random(10),
                'username' => Str::random(10),
                'email' => Str::random(10).'@gmail.com',
                'contact_number'=>Str::random(10),
                'address'=>Str::random(10),
                'role'=>1,
                'password' => Hash::make('secret'),
            ]);
        }
        $user5 = User::where('id', '!=', 5)->orderBy('id', 'desc')->limit(3)->get();
        foreach ($user5 as $user){
            $user->parrent_id = $firstuser->id;
            $user->save();
            DB::table('user_children')->insert([
                'user_id' => $firstuser->id,
                'child_id' => $user->id,
                'level' => '5',
            ]);
        }     
    }
}
