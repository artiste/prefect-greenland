<?php

namespace App\Mail;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Validator;

class AppMailer extends Controller
{
    protected $data;
    protected $senderName;
    protected $receiverName;
    protected $from;
    protected $to;
    protected $subject;
    protected $message;
    protected $view;
    protected $cc;
    protected $bcc;
    protected $attachments;
    protected $type;

    public function __construct($data)
    {
        $this->data = $data;
        $this->senderName = array_key_exists("senderName",$data) ? $data['senderName']:"";
        $this->receiverName = array_key_exists("receiverName",$data) ? $data['receiverName']:"";
        $this->from = array_key_exists("from",$data) ? $data['from']:"";
        $this->to = array_key_exists("to",$data) ? $data['to']:"";
        $this->subject = array_key_exists("subject",$data) ? $data['subject']:"";
        $this->message = array_key_exists("message",$data) ? $data['message']:"";
        $this->view = array_key_exists("view",$data) ? $data['view']:"";
        $this->cc = array_key_exists("cc",$data) ? $data['cc']:[];
        $this->bcc = array_key_exists("bcc",$data) ? $data['bcc']:[];
        $this->attachments = array_key_exists("attachments",$data) ? $data['attachments']:[];
        $this->type = array_key_exists("emailType",$data) ? $data['emailType']:"template";
    }
    public function send() {
        $validator = Validator::make($this->data, [
            'from' => 'required|email',
            'to' => 'required|email',
            'emailType' =>'required',
        ]);
        if ($validator->fails()) {
            return $validator->errors($validator);
        }else{
            $data = $this->data;
            if($this->type=='Basic'){
                $this->basicMail($data);
            }else if($this->type=='Template'){
                $this->templateMail($data);
            }else{
                $this->attachmentEmail($data);
            }
            return "Email Sent";
        }
    }
    //send basic mail
    public function basicMail($data) {
        Mail::send(['text'=> $this->view], $data, function($message) {
            $message->to($this->to,  $this->receiverName)->subject($this->subject);
            foreach ($this->cc as $cc){
                $message->cc($cc);
            }
            foreach ($this->bcc as $bcc){
                $message->bcc($bcc);
            }
            $message->from($this->from, $this->senderName);
        });
    }

    //send mail using template
    public function templateMail($data) {
        Mail::send($this->view, $data, function($message) {
            $message->to($this->to,  $this->receiverName)->subject($this->subject);
            foreach ($this->cc as $cc){
                $message->cc($cc);
            }
            foreach ($this->bcc as $bcc){
                $message->bcc($bcc);
            }
            $message->from($this->from, $this->senderName);
        });
    }

    //send mail using template and attachments
    public function attachmentEmail($data) {
        Mail::send($this->view, $data, function($message) {
            $message->to($this->to,  $this->receiverName)->subject($this->subject);
            foreach ($this->cc as $cc){
                $message->cc($cc);
            }
            foreach ($this->bcc as $bcc){
                $message->bcc($bcc);
            }
            $message->from($this->from, $this->senderName);
            foreach ($this->attachments as $attachments) {
                $message->attach($attachments);
            }
        });
    }

}
