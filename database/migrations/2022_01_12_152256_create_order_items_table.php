<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('sale_id')->unsigned()->nullable();
            $table->foreign("sale_id")->on('sales')->references('id')->cascadeOnDelete()->cascadeOnUpdate();
            $table->integer('line_items_id')->nullable();
            $table->bigInteger('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->on('products')->references('id')->cascadeOnUpdate()->cascadeOnDelete();
            $table->string("name")->nullable();
            $table->bigInteger('variation_id')->nullable();
            $table->decimal('subtotal',9,2)->nullable();
            $table->decimal('total',9,2)->nullable();
            $table->string('sku')->nullable();
            $table->integer('qty')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
