<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'header' => 'Login',
    //Dasboard
    'dashboard' => 'Dashboard',
    'Welcome To Perfect Greenland Dashboard' => 'Welcome To Perfect Greenland Dashboard',
    'dashboard_details' => 'Welcome To Perfect Greenland System',
    'dashboard_registered_user' => 'Registered users.',
    'dashboard_newlead' => 'Today new lead.', 

    'Reports' => 'Reports', 
    'Customers' => 'Customers', 
    'Sales' => 'Sales', 
    'Top Sales' => 'Top Sales', 
    'Orders' => 'Orders', 
    'Loading' => 'Loading', 
    
    //crud and pagination
    'actions'=>"actions",
    'preview'=>"Preview",
    'edit'=>"Edit",

    //sideBar
    'Dashboard' => 'Dashboard',
    'Leads'=>"Leads",
    'Users'=>"Users",
    'Drivers'=>"Drivers",
    'Orders'=>"Orders",

    //table heading for user and driver
    'Name'=>"Name",
    'Username'=> "Username",
    'Email Address'=>"Email Address",
    'contact_number'=>"contact_number",
    'Address'=>"Address",
    'Role'=>"Role",
    'Registered'=>"Registered On",

    //table heading for orders
    
    'Purchase Date'=>"Purchase Date",
    'Shipping Address'=>"Shipping Address",
    'Status'=>"Status",
    'Contact Address'=>"Contact Address",
    'contact number'=>"contact_number",
    'Payment Method'=>"Payment Method",
    'product listing'=>"product_listing",
    'shipping method'=>"shipping_method",
    'remark'=>"remark",
     'Status'=>"Status",
     'Amount'=>"Amount",
     'Created At'=>"Created At",
     'Updated At'=>"Updated At",
     'Number'=>   "Number",
     'Password'=> "Password",

     //order details
    'Date'=>"Date",    
    'Order Details'=>"Order Details",
    'Date'=>"Date", 
    'Status'=>"Status" ,
    'Name'=>"Name", 
    'email'=>"email", 
    'Contact Number'=>"Contact Number", 

    //dirver tasks
    'Drivers Tasks'=> "Drivers Tasks",
];
