<!-- text input -->
@php
    if($crud->entry==null){
        $lastSales = \Illuminate\Support\Facades\DB::table('sales')->select('invoice_number','id')->whereNotNull('invoice_number')->orderBy('created_at', 'desc')->first();
        if(!isset($lastSales)){
            $field['value'] ='#'.sprintf("%06s", 1);
        }else{
            $mmNextInvoice = $lastSales->invoice_number;
            $invoicLength = strlen($lastSales->invoice_number);
            $invoiceNumber = substr($lastSales->invoice_number, 2, $invoicLength);

            $nextInvoice = intval($invoiceNumber)+1;
            $field['value']= '#'.sprintf("%06s", $nextInvoice);
        }
    }
@endphp

@include('crud::fields.inc.wrapper_start')
<label>{!! $field['label'] !!}</label>
@include('crud::fields.inc.translatable_icon')

@if(isset($field['prefix']) || isset($field['suffix']))
    <div class="input-group"> @endif
        @if(isset($field['prefix']))
            <div class="input-group-prepend"><span class="input-group-text">{!! $field['prefix'] !!}</span></div> @endif
        <input
            type="text"
            name="{{ $field['name'] }}"
            value="{{ old(square_brackets_to_dots($field['name'])) ?? $field['value'] ?? $field['default'] ?? '' }}"
            @include('crud::fields.inc.attributes')
        >
        @if(isset($field['suffix']))
            <div class="input-group-append"><span class="input-group-text">{!! $field['suffix'] !!}</span></div> @endif
        @if(isset($field['prefix']) || isset($field['suffix'])) </div> @endif

{{-- HINT --}}
@if (isset($field['hint']))
    <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
    </div>
