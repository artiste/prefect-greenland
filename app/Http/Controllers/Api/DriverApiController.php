<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Sale;
use App\Models\SaleOrderRemark;
use App\Transformers\OrderTransformer;
use App\Transformers\UserTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Shipping;
use App\Models\Location;
use Codexshaper\WooCommerce\Models\Order;
use Codexshaper\WooCommerce\Facades\WooCommerce;
use Auth;
use App\Mail\AppMailer;
use function PHPUnit\Framework\isEmpty;

class DriverApiController extends Controller
{

    private $orderTransformer;


    /*
    * driver task list
    */
    /**
     * DriverApiController constructor.
     * @param $orderTransformer
     */
    public function __construct(OrderTransformer $orderTransformer)
    {
        $this->orderTransformer = $orderTransformer;
    }

    public function driverTaskList(Request $request)
    {
        $user = Auth::user();
        $taskList = collect();
        if ($request->has('date') && $request['date'] != null && $request['date'] != "") {
            $orders = Sale::where('assignee', $user->id)->WhereDate('delivery_date', $request['date'])->get();
        } else {
            $orders = Sale::where('assignee', $user->id)->whereNotIn('status',[Sale::STATUS_COMPLETED,Sale::STATUS_CANCELED])->orderBy('delivery_date','desc')->get();
        }

        foreach ($orders as $order) {
            switch ($order->status) {
                case \App\Models\Sale::STATUS_PROCESSING:
                    $mstatus = "pending";
                    break;

                case \App\Models\Sale::STATUS_CURRENT_DELIVERY:
                    $mstatus = "In Delivery";
                    break;
                default:
                    $mstatus = $order->status;
                    break;

            }


            $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $order->created_at);
            $data['order_id'] = $order->id;
            $data['purchase_date'] = $order->date_paid;
            $data['status'] = $mstatus;
            $data['amount'] = $order->total;
            $data['payment_method'] = $order->payment_method;
            $data['customer_name'] = $order->customer->name;
            $data['customer_phone'] = $order->customer->contact_number;
            $data['customer_email'] = $order->customer->email;
            $data['date'] = $carbon->format('d-m-Y');
            $data['time'] = $carbon->format('g:i A');;
            $taskList->add($data);
        }
        return responder()->success($taskList)->respond();
    }

    /*
    * driver task details
    */
    public function driverTaskDetail($taskId, Request $request)
    {
        $order = Sale::find($taskId);

        $customer = $order->customer;

        $data['order_id'] = $order->id;
        $data['customer_id'] = $customer->id;
        $data['name'] = $customer->name;
        $data['avatar'] = $customer->getMedia('profile_image')->first() == null ? null : $customer->getMedia('profile_image')->first()->getUrl();
        $data['phone_number'] = $customer->contact_number;
        switch ($order->status) {
            case \App\Models\Sale::STATUS_PROCESSING:
                $mstatus = "pending";
                break;

            case \App\Models\Sale::STATUS_CURRENT_DELIVERY:
                $mstatus = "In Delivery";
                break;
            default:
                $mstatus = $order->status;
                break;
        }

        $data['status'] = $mstatus;
        $paidDate = $order->date_paid;
        if (!isset($paidDate)) {
            $data['date'] = null;
            $data['time'] = null;

        } else {
            $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $paidDate);
            $data['date'] = $carbon->format('d-m-Y');
            $data['time'] = $carbon->format('g:i A');

        }

        $shippingAddress = $order->shippingAddress->address_1 . ' , ' . $order->shippingAddress->address_2 . ' , ' .
            $order->shippingAddress->city . ' , ' . $order->shippingAddress->postcode . ' , ' .
            $order->shippingAddress->state . ' , ' . $order->shippingAddress->country;
        $data['location'] = $shippingAddress;
        $data['shipping_total'] = $order->shipping_total;
        $data['total'] = $order->total;

        $remarkList = $order->remark()->orderBy("created_at", 'DESC')->first();
        if ($remarkList != null) {
            $data['remark'] = $remarkList->remark;
        }


        //ProductList
        $productList = collect();
        foreach ($order->items as $item) {
            $child['name'] = $item->name;
            $child['quantity'] = $item->quantity;
            $child['price'] = $item->price;
            $productList->push($child);
        }

        $data['lineItem'] = $productList;

        return responder()->success($order, $this->orderTransformer)->respond();
    }

    /*
    * Save driver latitude and longtitude
    */
    public function syncLocation(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'lat' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'lng' => ['required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
        ]);
        if ($validator->fails()) {
            $response = $validator->errors();
        } else {
            $location = Location::create([
                'user_id' => $user->id,
                'lat' => $request['lat'],
                'lng' => $request['lng'],
            ]);
            $response = $location;
        }
        return responder()->success($response)->respond();
    }

    /*
    * Change order status and put remark
    */
    public function updateOrder($taskId, Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
//            'order_id' => 'required|numeric',
        ]);
        $sales = null;
        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return responder()->error(500, $error);
        } else {
            //Get the requested order from admin panel database
            $sales = Sale::find($taskId);

            //Check if driver change order status
            if ($request->has('status') && $request['status'] != '') {
                $status = $request['status'];
            } else {
                $status = "";
            }
            //Check if driver make any remark
            if ($request->has('remark') && $request['remark'] != '') {
                $remark = $request['remark'];
            } else {
                $remark = "";
            }


            //Set remark text
//            switch ($remark) {
//                case "":
//                    if ($status != "") {
//                        $remarkText = 'Order status is changed to ' . $status;
//                    } else {
//                        $remarkText = '';
//                    }
//                    break;
//                default:
//                    if ($status != "") {
//                        $remarkText = $remark . '. And order status is changed to ' . $status;
//                    } else {
//                        $remarkText = $remark;
//                    }
//            }

            //Check if driver upload any image
//            if ($request->hasFile('file')) {
//                $validator = Validator::make($request->all(), [
//                    'file' => 'image|mimes:jpeg,png,jpg|max:2048',
//                ]);
//                if ($validator->fails()) {
//                    $response = $validator->errors();
//                } else {
//                    $file = $request->file('file');
//                    $image = $file->getClientOriginalName();
//                    $extension = pathinfo($image, PATHINFO_EXTENSION);
//                    $filename = $sales->id . '-' . time() . '.' . $extension;
//                    // $filename = pathinfo($image, PATHINFO_FILENAME);
//
//                    //Set remark image folder
//                    $imageFolder = '/uploads/images/orderRemarks/';
//
//                    //Create remark
//                    $remarkCreate = OrderRemark::create([
//                        'driver_id' => $user->id,
//                        'order_id' => $request['order_id'],
//                        'remark' => $remarkText,
//                        'image' => $imageFolder . $filename
//                    ]);
//
//                    //Set path for mage
//                    $path = public_path() . $imageFolder;
//                    //Move the image to folder
//                    $file->move($path, $filename);
//
//                    //Response for API return
//                    $response = $remarkCreate;
//                }
//            } else {

            //Create remark
            $remarkCreate = SaleOrderRemark::create([
                'driver_id' => $user->id,
                'order_id' => $request['order_id'],
                'remark' => $remark
            ]);
            $response = $remarkCreate;
//            }

            //check if the order status is not empty and is simmilar to the existing in the DB
            if ($status != '' && $status != $sales->status) {
                switch ($status) {
                    case  "pending":
                        $mstatus = \App\Models\Sale::STATUS_PROCESSING;
                        break;

                    case "In Delivery":
                        $mstatus = \App\Models\Sale::STATUS_CURRENT_DELIVERY;
                        break;
                    default:
                        $mstatus = $status;
                        break;
                }


                $orderId = $sales->wordpress_order_id;

                //Change status in admin panel
                $sales->status = $mstatus;
                $sales->saveImage($request);
                $sales->Save();

                $status = $mstatus;
                $data = [
                    'status' => $status,
                ];
                //Update status in wordpress website
//                $order = Sale::update($orderId, $data);
//                if ($sales && $order) {
//                    $this->emailOrder($sales, $mstatus);
//                }
            }
        }
        return responder()->success($sales, $this->orderTransformer)->respond();
    }

    //Send email to customer on order update
    public function emailOrder($sales, $status)
    {
        $customer = $sales->customer;
        $data = [
            'senderName' => 'PerfectGreenland', //sender name optional field
            'receiverName' => $customer->name, //receiver name optional field
            'from' => config('mail.mailers.smtp.username'),//sender address mandatory field
            'to' => 'ahmadh24.r@gmail.com',//receiver address mandatory field
            'subject' => 'Order Status Update',//email subject optional field
            // 'emailBody' => 'Sending Mail from Laravel.',//email body optional field can place in template as well
            'view' => 'emails.orderUpdate', //view mandatory field
            'cc' => [], //cc to multiple emails optional array array field
            'bcc' => [], //bcc to multiple emails optional field
            'emailType' => 'Template', //email type can be Basic, Template, Attachment, . Basic email Does not support html templates  it is faster than template and attachments type
            'order_id' => $sales->wordpress_order_id,
            'status' => $status,
        ];
        $email = new AppMailer($data);
        $message = $email->send();
    }

    /*
    * Update profile
    */
    public function updateProfile(Request $request)
    {
        $user = Auth::user();
        //Check if driver upload any image
        if ($request->hasFile('image')) {
            $validator = Validator::make($request->all(), [
                'image' => 'image|mimes:jpeg,png,jpg|max:2048',
                'name' => 'required',
                'contact_number' => 'required',
                'address' => 'required',
                'car_plate' => 'required',
                'driving_license' => 'required',
            ]);
            if ($validator->fails()) {
                $response = $validator->errors();
            } else {
                $file = $request->file('image');
                $image = $file->getClientOriginalName();
                $extension = pathinfo($image, PATHINFO_EXTENSION);
                $filename = $user->id . '.' . $extension;

                //Set remark image folder
                $imageFolder = public_path() . '/uploads/images/driverProfile/';

                //Update driver information
                $driver = $user->update([
                    'name' => $request['name'],
                    'contact_number' => $request['contact_number'],
                    'address' => $request['address'],
                    'car_plate' => $request['car_plate'],
                    'driving_license' => $request['driving_license']
                ]);

                $user->addMediaFromRequest('image')->toMediaCollection('profile_image');


                //Set path for image
                $path = $imageFolder;
                //Move the image to folder
                $file->move($path, $filename);

                //Response for API return
                $user = Auth::user();
                $response = $user;
            }
        } else {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'contact_number' => 'required',
                'address' => 'required',
                'car_plate' => 'required',
                'driving_license' => 'required',
            ]);
            if ($validator->fails()) {
                $response = $validator->errors();
            } else {
                //Update driver information
                $driver = $user->update([
                    'name' => $request['name'],
                    'contact_number' => $request['contact_number'],
                    'address' => $request['address'],
                    'car_plate' => $request['car_plate'],
                    'driving_license' => $request['driving_license'],
                ]);
                $user = Auth::user();
                $response = $user;
            }
        }
        return responder()->success($response)->respond();
    }

    public function getDriverProfile(Request $request)
    {
        $user = Auth::user();
        return responder()->success($user, new UserTransformer())->respond();
    }

}
