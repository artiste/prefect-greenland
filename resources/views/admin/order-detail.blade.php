@extends(backpack_view('blank'))
@section('header')
    <section class="container-fluid">
        <h2 style="align-content: center; text-align: center">
            <span class="text-capitalize"><br> Order Details Report</span>
        </h2>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </section>
@endsection

@section('content')
@php
$id = request('id');
$order = App\Models\Order::where('id', $id)->first();
$shipping = $order->shipping;
$order_item= $order->lineItem;
@endphp
<!------ Include the above in your HEAD tag ---------->
<body> 
<div class="container">                          
    <div class="row">
        <div class="col-md-6" style= "padding: 15px;">
            <table>
                <tr>
                    <td colspan="2"> Order Details</td>
                </tr>
                <tr>
                    <td> Date: </td><td>{{$order->created_at->format('M- d - Y')}}</td>
                </tr>
                <tr>
                    <td> Status: </td><td>{{$order->status}}</td>
                </tr>
                {{-- <tr>
                    <td colspan="2"> Customer Details</td>
                </tr> --}}
                <tr>
                    <td> Name: </td><td>{{$order->user->name}}</td>
                </tr>
                <tr>
                    <td> email: </td><td>{{$order->user->email}}</td>
                </tr>
                <tr>
                    <td> Contact Number: </td><td>{{$order->user->contact_number}}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-4" style= "padding: 15px;">
            <table>
                <tr>
                    <td colspan="2"> Shipping address</td>
                </tr>
                @if($shipping)
                <tr>
                    <td> Address Line 1: </td><td>{{$shipping->address_1}}</td>
                </tr>
                <tr>
                    <td> Address Line 2: </td><td>{{$shipping->address_2}}</td>
                </tr>
                <tr>
                    <td> City: </td><td>{{$shipping->city}}</td>
                </tr>
                <tr>
                    <td> State:</td><td>{{$shipping->state}}</td>
                </tr>  
                <tr>
                    <td> Post Code:</td><td>{{$shipping->postcode}}</td>
                </tr>
                @else
                <tr>
                    <td colspan="2" style="color:gray; font-size:10pt;">Shippin Address not set</td>
                </tr>
                @endif
            </table>
        </div>  
        <div class="col-md-12" style= "padding: 15px;">
            <table id="orderTable" class="table table-display responsive nowrap" width="100%">
                <thead>
                    <tr>
                        <th colspan="" style="width:500px;text-align:left">Item</th>
                        <th style="text-align:right">Cost</th>
                        <th style="text-align:center">Quantiy</th>
                        <th style="text-align:center">Total</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $total =0;
                    @endphp
                @foreach($order_item as $orderr)
                <tr>
                    <td style="text-align:left">{{$orderr['name']}}</td>
                    <td style="text-align:right">{{$orderr['price']}}</td>
                    <td style="text-align:center">{{$orderr['quantity']}}</td>
                    <td style="text-align:center">{{($orderr->price * $orderr->quantity) }}</td>
                    @php
                    $total +=$orderr->price * $orderr->quantity ;
                    @endphp
                </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" style="font-weight: bold; text-align:right">Item SubTotal</td>
                        <td style="font-weight: bold; text-align:center"> {{$total}}</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="font-weight: bold; text-align:right">Total Tax</td>
                        <td style="font-weight: bold; text-align:center">{{$order->total_tax}}</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="font-weight: bold; text-align:right">Shipping Fee</td>
                        <td style="font-weight: bold; text-align:center">{{$order->shipping_total}}</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="font-weight: bold; text-align:right">Order Total</td>
                        <td style="font-weight: bold; text-align:center">{{$order->shipping_total + $order->total_tax + $total}}</td>
                    </tr>
                </tfoot>
            </table>
        </div>                  
        {{--<div class="col-md-6" style= "padding: 15px;">
            @php
                $users = App\Models\User::where('role', App\Models\User::ROLE_DRIVER )->get();
            @endphp 
            <form class="form-inline" action="{{route('assignDriver')}}" method="POST">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="assignee">Driver &nbsp;</label>
                    <select class="custom-select my-1" id="assignee" name="assignee" required>
                        <option value="">--Select Driver--</option>
                        @foreach($users as $driver)
                            <option value="{{$driver->id}}" @if($order->assignee == $driver->id) selected @endif>{{$driver->name}}</option>
                        @endforeach
                    </select>
                    <input type="hidden" name= "order_id" value="{{$order->id}}">
                    &nbsp; &nbsp;<input type="submit" value="Assign" class="btn btn-primary">
                </div>
            </form>           
        </div> --}}  
        <div class="col-md-6" style= "padding: 15px;">
            @php
                $allStatus = App\Models\Order::STATUS;
            @endphp 
            <form class="form-inline" action="{{route('updateStatus')}}" method="POST">
            {{ csrf_field() }}
                <div class="form-group">
                    <label for="assignee">Status &nbsp;</label>
                    <select class="custom-select my-1" name="status" id="status" required>
                        @foreach($allStatus as $status)
                            <option value="{{$status}}" @if($order->status == $status) selected @endif>{{$status}}</option>
                        @endforeach
                    </select>
                    <input type="hidden" name= "order_id" value="{{$order->id}}">
                    &nbsp; &nbsp;<input type="submit" value="Save" class="btn btn-primary">
                </div>
            </form>           
        </div>  
    </div>
</div>
</body><br><br>
<style>
body {
    background-color: transparent;
    border: 0px solid;
}
</style> 
@endsection

