<?php

namespace App\Transformers;

use App\Models\Sale;
use Carbon\Carbon;
use Flugg\Responder\Transformers\Transformer;

class OrderTransformer extends Transformer
{

    private $lineItemTransformer;



    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [
        'lineItem'
    ];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [
        'lineItem'
    ];

    /**
     * OrderTransformer constructor.
     * @param $lineItemTransformer
     */public function __construct(LineItemTransformer $lineItemTransformer){
        $this->lineItemTransformer = $lineItemTransformer;
     }



    /**
     * Transform the model.
     *
     * @param \App\Sale $order
     * @return array
     */
    public function transform(Sale $order)
    {

        $customer = $order->customer;

        switch ($order->status) {
            case \App\Models\Sale::STATUS_PROCESSING:
                $mstatus = "pending";
                break;

            case \App\Models\Sale::STATUS_CURRENT_DELIVERY:
                $mstatus = "In Delivery";
                break;
            default:
                $mstatus = $order->status;
                break;
        }



        $paidDate = $order->date_paid;
        if (!isset($paidDate)) {
            $data['date'] = null;
            $data['time'] = null;

        } else {
            $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $paidDate);
            $data['date'] = $carbon->format('d-m-Y');
            $data['time'] = $carbon->format('g:i A');
        }

        $carbon = null;
        if(isset($paidDate)){
            $carbon = Carbon::createFromFormat('Y-m-d H:i:s', $paidDate);
        }

        $shippingAddress = $order->shippingAddress->address_1 . ' , ' . $order->shippingAddress->address_2 . ' , ' .
            $order->shippingAddress->city . ' , ' . $order->shippingAddress->postcode . ' , ' .
            $order->shippingAddress->state . ' , ' . $order->shippingAddress->country;


        $remarkList = $order->remark()->orderBy("created_at", 'DESC')->first();
        if ($remarkList != null) {
            $data['remark'] = $remarkList->remark;
        }

        return [
            'order_id' => (int)$order->id,
            'customer_id' => $customer->id,
            'name' => $customer->name,
            'avatar' => $customer->getMedia('profile_image')->first() == null ? null : $customer->getMedia('profile_image')->first()->getUrl(),
            'phone_number' => $customer->contact_number,
            'status' => $mstatus,
            'date' => !isset($carbon)?null:$carbon->format('d-m-Y'),
            'time' => !isset($carbon)?null:$carbon->format('g:i A'),
            'location' => $shippingAddress,
            'shipping_total' => $order->shipping_total,
            'total' => $order->total,
            'remark' => $remarkList == null ? null : $remarkList->remark,
            'image' => $order->getImageURL()
        ];
    }

    public function includeLineItem(Sale $order)
    {
        $lineItem = $order->items;
        return $this->collection($lineItem, $this->lineItemTransformer);
    }
}
