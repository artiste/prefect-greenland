<?php

namespace App\Http\Controllers;

use App\Models\Customer as Client;
use Illuminate\Http\Request;
use App\Models\Order as Sales;
use App\Models\User;
use App\Models\Shipping;
use App\Models\Billing;
use App\Models\LineItem;
use Carbon\Carbon;

//wooCommerce models and classes
use Codexshaper\WooCommerce\Models\Product;
use Codexshaper\WooCommerce\Models\Customer;
use Codexshaper\WooCommerce\Models\Order;
use Codexshaper\WooCommerce\Facades\WooCommerce;
use Illuminate\Support\Facades\Log;

class WoocommerceController extends Controller
{

    // public $consumerKey='ck_121406fa2be2cd25d7001382eedb9321b629fdc5';
    // public $consumerSecret='cs_c39d6615a8de8b15a71d2d1c94e397e6cd56e619';

    /**
     * This function sync the user from wordpress into users table
     */
    /**
     * This function sync the user from wordpress into users table
     */
    public function syncUser(Request $request)
    {
        switch ($request['type']) {
            case 'create':
            case 'update':
                $customer = Customer::find($request['wordpress_user_id']);

                $mAddress = $customer['billing']->address_1.', '.$customer['billing']->address_2.", ".$customer['billing']->city." ".$customer['billing']->postcode;
                $checkUser = User::where('wordpress_user_id', $request['wordpress_user_id'])->first();
                if(isset($checkUser)){
                    User::where('wordpress_user_id', $request['wordpress_user_id'])
                        ->update([
                            'name'=>$customer['first_name'].' '.$customer['last_name'],
                            'username'=>$customer['username'],
                            'password'=>$request['password'],
                            'email'=>$customer['email'],
                            'contact_number'=>$customer['billing']->phone,
                            'address'=>$mAddress,
                            'role'=>2,
                            'wordpress_user_id'=>$request['wordpress_user_id'],
                        ]);
                }else{
                    $user = User::create([
                        'name'=>$customer['first_name'].' '.$customer['last_name'],
                        'username'=>$customer['username'],
                        'password'=>$request['password'],
                        'email'=>$customer['email'],
                        'contact_number'=>$customer['billing']->phone,
                        'address'=>$mAddress,
                        'role'=>2,
                        'wordpress_user_id'=>$request['wordpress_user_id'],
                    ]);
                }

//                $user = User::create($request->all());
                break;

//            case 'update':
//                $user = User::where('wordpress_user_id', $request['wordpress_user_id'])->first();
//                $user->name = $request['name'];
//                $user->email = $request['email'];
//                $user->password = $request['password'];
//                $user->created_at = $request['created_at'];
//                $user->wordpress_user_id = $request['wordpress_user_id'];
//                $user->save();
//                break;
        }
        return responder()->success($user, null)->respond();
    }

    /**
     * This function gets the completed order from wordpress
     * Sync the order to order table
     * This function also calculate commissions for users
     */
    public function orderComplete(Request $request)
    {
        //Order id recieved via API
        $order_id = $request['order_id'];

        //Get order data from wordpress order API
        $order = Order::find($order_id);
    //    dd($order);
        //Get the user data belongs to order from order table
        $user = User::where('wordpress_user_id', $order['customer_id'])->first();
        if (!isset($user)) {
            $WooCustomer = Customer::find($order['customer_id']);
            if (isset($WooCustomer)) {
                $mAddress = $WooCustomer['billing']->address_1.', '.$WooCustomer['billing']->address_2.", ".$WooCustomer['billing']->city." ".$WooCustomer['billing']->postcode;
                $user = User::create([
                    'name'=>$WooCustomer['first_name'].' '.$WooCustomer['last_name'],
                    'username'=>$WooCustomer['username'],
                    'password'=>$request['password'],
                    'email'=>$WooCustomer['email'],
                    'contact_number'=>$WooCustomer['billing']->phone,
                    'address'=>$mAddress,
                    'role'=>2,
                    'wordpress_user_id'=>$WooCustomer['id'],
                ]);
                $user->save();
            }
        }





        //Get sales if already exists
        $sales = Sales::where('wordpress_order_id', $order['id'])->first();

        if($sales){
            // Save order in order in order table
            $sales->wordpress_order_id = $order['id'];
            $sales->order_key = $order['order_key'];
            $sales->status = $order['status'];
            $sales->currency = $order['currency'];
            $sales->discount_total = $order['discount_total'];
            $sales->discount_tax = $order['discount_tax'];
            $sales->shipping_total = $order['shipping_total'];
            $sales->shipping_tax = $order['shipping_tax'];
            $sales->total = $order['total'];
            $sales->total_tax = $order['total_tax'];
            $sales->prices_include_tax = $order['prices_include_tax'];
            $sales->wordpress_customer_id = $order['customer_id'];
            $sales->customer_ip_address = $order['customer_ip_address'];
            $sales->customer_note = $order['customer_note'];
            $sales->payment_method = $order['payment_method'];
            $sales->transaction_id = $order['transaction_id'];
            $sales->date_paid = $order['date_paid'];
            $sales->date_completed = $order['date_completed'];
            $sales->user_id = $user->id;
            $sales->save();

            }else{
            // Save order in order in order table
            $sales = Sales::create([
                'wordpress_order_id'=>$order['id'],
                'order_key'=>$order['order_key'],
                'status'=>$order['status'],
                'currency'=>$order['currency'],
                'discount_total'=>$order['discount_total'],
                'discount_tax'=>$order['discount_tax'],
                'shipping_total'=>$order['shipping_total'],
                'shipping_tax'=>$order['shipping_tax'],
                'total'=>$order['total'],
                'total_tax'=>$order['total_tax'],
                'prices_include_tax'=>$order['prices_include_tax'],
                'wordpress_customer_id'=>$order['customer_id'],
                'customer_ip_address'=>$order['customer_ip_address'],
                'customer_note'=>$order['customer_note'],
                'payment_method'=>$order['payment_method'],
                'transaction_id'=>$order['transaction_id'],
                'date_paid'=>$order['date_paid'],
                'date_completed'=>$order['date_completed'],
                'user_id'=>$user->id,
            ]);

            //Get line Items
            $orderItems = $order['line_items'];

            //Save the line items
            foreach ($orderItems as $item){
                LineItem::create([
                    'name'=>$item->name,
                    'order_id'=>$sales->id,
                    'quantity'=>$item->quantity,
                    'tax_class'=>$item->tax_class,
                    'subtotal'=>$item->subtotal,
                    'subtotal_tax'=>$item->subtotal_tax,
                    'total'=>$item->total,
                    'total_tax'=>$item->total_tax,
                    'sku'=>$item->sku,
                    'price'=>$item->price,
                    'wp_id'=>$item->id,
                    'wp_order_id'=>$order['id'],
                    'wp_product_id'=>$item->product_id,
                    'wp_variation_id'=>$item->variation_id,
                ]);
            }
             //Get BIlling address
             $billing = (empty($order['billing'])) ? $order['shipping'] : $order['billing'];
             //Save the biling
                if(!empty($billing)){
                    Billing::create([
                        'user_id'=>$user->id,
                        'order_id'=>$sales->id,
                        'address_1'=>$billing->address_1,
                        'address_2'=>$billing->address_2,
                        'city'=>$billing->city,
                        'state'=>$billing->state,
                        'postcode'=>$billing->postcode,
                        'country'=>$billing->country,
                        'email'=>$billing->email,
                        'phone'=>$billing->phone,
                    ]);
                }

               //Get line Items
               $shipping = (empty($order['shipping']->address_1) && empty($order['shipping']->address_2)) ? $order['billing'] : $order['shipping'];

               //Save the line items
                    if(!empty($shipping)){
                        Shipping::create([
                            'user_id'=>$user->id,
                            'order_id'=>$sales->id,
                            'address_1'=>$shipping->address_1,
                            'address_2'=>$shipping->address_2,
                            'city'=>$shipping->city,
                            'state'=>$shipping->state,
                            'postcode'=>$shipping->postcode,
                            'country'=>$shipping->country,
                        ]);
                    }else{
                        Shipping::create([
                            'user_id'=>$user->id,
                            'order_id'=>$sales->id,
                            'address_1'=>$billing->address_1,
                            'address_2'=>$billing->address_2,
                            'city'=>$billing->city,
                            'state'=>$billing->state,
                            'postcode'=>$billing->postcode,
                            'country'=>$billing->country,
                        ]);
                    }
        }
        //return response
        return responder()->success($sales, null)->respond();
    }

}
