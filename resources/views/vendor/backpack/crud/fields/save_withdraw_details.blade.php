@php
$status = App\Models\Withdraw::STATUS;
$current_status = $crud->entry->status;
@endphp

@if((backpack_user()->role==App\Models\User::ROLE_MEMBER && ($current_status == $status['processing']||$current_status == $status['reject']|| 
$current_status == $status['approve']))||(backpack_user()->role!=App\Models\User::ROLE_MEMBER 
&& $current_status == $status['cancel']))
@else
<div class="form-group col-sm-12 pull-right">
@include('crud::inc.form_save_buttons')
</div>
@endif