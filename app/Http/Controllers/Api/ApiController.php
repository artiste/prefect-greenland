<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Auth;

class ApiController extends Controller
{
    // public function register(Request $request)
    // {
    //     /**Validate the data using validation rules
    //     */
    //     $validator = Validator::make($request->all(), [
    //         'name' => 'required',
    //         'email' => 'required|email',
    //         'password' => 'required',
    //     ]);

    //     /**Check the validation becomes fails or not
    //     */
    //     if ($validator->fails()) {
    //         /**Return error message
    //         */
    //         return response()->json([ 'error'=> $validator->errors() ]);
    //     }

    //     /**Store all values of the fields
    //     */
    //     $newuser = $request->all();

    //         /**Create an encrypted password using the hash
    //     */
    //     $newuser['password'] = Hash::make($newuser['password']);

    //     /**Insert a new user in the table
    //     */
    //     $user = User::create($newuser);

    //         /**Create an access token for the user
    //     */
    //     $success['token'] = $user->createToken('AppName')->accessToken;
    //     /**Return success message with token value
    //     */
    //     return response()->json(['success'=>$success], 200);
    // }

    public function login(Request $request)
    {
        /**Read the credentials passed by the user
        */
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        /**Check the credentials are valid or not
        */
        if(auth()->attempt($credentials)){
            /*
             *Store the information of authenticated user
            */
            $user = Auth::user();
            if($user->status == User::STATUS_INACTIVE){
                return responder()->error(500,'Sorry, your account is inactive. Please contact your administrator.');
            }
            /**Create token for the authenticated user
            */
            $success['token'] = $user->createToken('AppName')->accessToken;
            $result = array('token'=>$success['token'], 'user'=>$user);
            return responder()->success($result)->respond();
            // return response()->json(['success' => $success, 'user'=>$user], 200);
        } else {
            /*
             *Return error message
            */
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

     /*
    * Update user app_token
    */
    public function syncUserAppToken(Request $request){
        $validator = Validator::make($request->all(), [
            'wordpress_user_id' => ['required'],
            'app_token' => ['required']
        ]);
        if ($validator->fails()) {
            $response = $validator->errors();
        }else{
            $user = User::where('wordpress_user_id', $request['wordpress_user_id'])->first();
            if($user){
                $user->update([
                    'app_token'=>$request['app_token']
                ]);
                $response = $user;
            }else{
                $response = ['User is not found'];
            }
        }
        return responder()->success($response)->respond();
    }

}
