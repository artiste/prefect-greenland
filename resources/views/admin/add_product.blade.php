@extends(backpack_view('blank'))

@section('header')
    <section class="container-fluid">
		<meta charset="utf-8">
		<title>Add product</title>
		<meta name="description" content="add new product.">
		<meta name="author" content="Łukasz Holeczek">
		<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
		<!-- end: Meta -->
		
		<!-- start: Mobile Specific -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
    </section></br>
@endsection
@php
    $categories = App\Models\Category::all();
@endphp
@section('content')
<div class="container" style="background-color: white;">
	<section class="panel panel-default">
		<div class="panel-heading"> 
			<h3 class="panel-title">Add New Product</h3> 
		</div> 
		<p class="alert-success">
			<?php
			$message=Session::get('message');
			if($message)
			{
				echo $message;
				Session::put('message',null);

			}
           ?>
		</p>
		<div class="panel-body">
		<form action="{{route('SaveProduct')}}" method="POST" enctype = "multipart/form-data">
                 {{ csrf_field() }}
				<div class="form-group">
					<label for="name" class="col-sm-3 control-label"required>Product Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="name" id="name">
					</div>
				</div>
				<div class="form-group">
					<label for="name" class="col-sm-3 control-label"required>Product Description</label>
					<div class="col-sm-9">
						<textarea class="form-control"name="description" id="description" ></textarea>		
					</div>
				</div> 
				<div class="form-group">
					<label for="about" class="col-sm-3 control-label"required>Quantity</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="quantity" id="quantity"required>
					</div>
				</div> 
				<div class="form-group">
					<label for="qty" class="col-sm-3 control-label"required>price</label>
					<div class="col-sm-3">
						<input type="text" class="form-control" name="price" id="price">
					</div>
				</div> 
				<div class="form-group">
					<span id="date-label-from" class="date-label">Category Name:</span>
					<select class="selectpicker"name="category" id="category" style="padding-right:75px;padding-bottom:5px;padding-top:4px;">
						<option value="">--Select--</option>
						@foreach($categories as $category){
							<option value="{{$category->id}}">{{$category->category_name}}</option>
						@endforeach
					</select>
				</div>
				<div class="col-sm-3">   
					<label class="control-label small" for="in_stok">In Stock:</label>
					<input type="text" class="form-control" name="in_stock" id="in_stock">
				</div>
				<div class="col-sm-3">   
					<label class="control-label small" for="in_stok">Date:</label>
					<input type="date" class="form-control" name="created_at" id="created_at">
				</div>
				<div class="control-group">
				  <label class="control-label" for="fileInput">Image </label>
				  <div class="controls">
					<input class="input-file uniform_on" name="product_image" id="fileInput" type="file">
				  </div>
				</div>   <hr>	
				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Add Product </button>
					<button type="reset" class="btn">Cancel</button>
				</div></br>
			</form>  
		</div>
	</section> 
</div>
@endsection
@push('after_scripts')

<script>
	$(document).ready(function(){
	// jquery code here
	
	});
</script>
 @endpush