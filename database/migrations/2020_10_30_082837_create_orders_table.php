<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned()->nullable()->index();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->bigInteger('assignee')->unsigned()->nullable()->index();
            $table->foreign('assignee')
                ->references('id')->on('users');
            $table->decimal('amount', 8, 2)->nullable();
            $table->bigInteger('wordpress_order_id')->nullable();
            $table->string('order_key')->nullable();
            $table->string('status')->nullable();
            $table->string('currency')->nullable();
            $table->decimal('discount_total',9,2)->nullable()->default(0);
            $table->decimal('discount_tax',9,2)->nullable()->default(0);
            $table->decimal('shipping_total',9,2)->nullable()->default(0);
            $table->decimal('shipping_tax',9,2)->nullable()->default(0);
            $table->decimal('total',9,2)->nullable()->default(0);
            $table->decimal('total_tax',9,2)->nullable()->default(0);
            $table->boolean('prices_include_tax')->default(false);
            $table->bigInteger('wordpress_customer_id')->nullable();
            $table->string('customer_ip_address')->nullable();
            $table->string('customer_note')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('transaction_id')->nullable();
            $table->dateTime('date_paid')->nullable();
            $table->dateTime('date_completed')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
