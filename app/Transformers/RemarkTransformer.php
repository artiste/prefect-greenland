<?php

namespace App\Transformers;

use App\Remark;
use Flugg\Responder\Transformers\Transformer;

class RemarkTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\Remark $remark
     * @return array
     */
    public function transform(Remark $remark)
    {
        return [
            'id' => (int) $remark->id,
        ];
    }
}
