<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddParameterIntoUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('car_plate')->nullable()->after('wordpress_user_id');
            $table->string('driving_license')->nullable()->after('wordpress_user_id');
            $table->string('app_token')->nullable()->after('wordpress_user_id');
            $table->text('image')->nullable()->after('wordpress_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('car_plate');
            $table->dropColumn('driving_license');
            $table->dropColumn('app_token');
            $table->dropColumn('image');
        });
    }
}
