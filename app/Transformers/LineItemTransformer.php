<?php

namespace App\Transformers;

use App\Models\LineItem;
use App\Models\OrderItem;
use Flugg\Responder\Transformers\Transformer;

class LineItemTransformer extends Transformer
{
    /**
     * List of available relations.
     *
     * @var string[]
     */
    protected $relations = [];

    /**
     * List of autoloaded default relations.
     *
     * @var array
     */
    protected $load = [];

    /**
     * Transform the model.
     *
     * @param  \App\OrderItem $lineItem
     * @return array
     */
    public function transform(OrderItem $lineItem)
    {
        return [
            'id' => (int) $lineItem->id,
            'name' => $lineItem->name,
            'quantity' =>$lineItem->qty,
            'price'=>$lineItem->total
        ];
    }
}
