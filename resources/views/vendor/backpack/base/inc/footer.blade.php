@if (config('backpack.base.show_powered_by') || config('backpack.base.developer_link') || config('backpack.base.system_version'))
    <div class="text-muted ml-auto mr-auto">
      @if (config('backpack.base.developer_link') && config('backpack.base.developer_name'))
      {{ trans('backpack::base.handcrafted_by') }} <a target="_blank" rel="noopener" href="{{ config('backpack.base.developer_link') }}">{{ config('backpack.base.developer_name') }}</a>.
      @endif
      @if (config('backpack.base.show_powered_by'))
      {{ trans('backpack::base.powered_by') }} <a target="_blank" rel="noopener" href="http://backpackforlaravel.com?ref=panel_footer_link">Backpack for Laravel</a>.
      @endif
      <div class="col-md-0"style= "color: white">
          @if (config('backpack.base.system_version'))
          {{ trans('Version') }} <a target="_blank" rel="noopener" href="http://backpackforlaravel.com?ref=panel_footer_link">1.1</a>
          @endif
      </div>

    </div>
@endif
