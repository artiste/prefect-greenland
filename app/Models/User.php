<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Backpack\CRUD\app\Models\Traits\CrudTrait as CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class User extends Authenticatable implements HasMedia
{
    use HasApiTokens, HasFactory, Notifiable, CrudTrait;
    use InteractsWithMedia;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'password',
        'email',
        'contact_number',
        'address',
        'role',
        'email_verified_at',
        'wordpress_user_id',
        'car_plate',
        'driving_license',
        'app_token',
        'image',
        'status',
    ];
     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    CONST ROLE_ADMIN = '1';
    CONST ROLE_CUSTOMER = '2';
    CONST ROLE_DRIVER = '3';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */

  /*
  |--------------------------------------------------------------------------
  | RELATIONS
  |--------------------------------------------------------------------------
  */
    public function order()
    {
        return $this->hasMany(Order::class, 'user_id');
    }


    public function driverOrder()
    {
        return $this->hasMany(Order::class, 'assignee');
    }

    public function shipping_address()
    {
        return $this->hasMany(Shipping::class, 'user_id');
    }

    public function billing_address()
    {
        return $this->hasMany(Billing::class, 'user_id');
    }

    public function location()
    {
        return $this->hasMany(Location::class, 'user_id');
    }

  /*
  |--------------------------------------------------------------------------
  | SCOPES
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | ACCESORS
  |--------------------------------------------------------------------------
  */

  /*
  |--------------------------------------------------------------------------
  | MUTATORS
  |--------------------------------------------------------------------------
  */
  public function setImageAttribute($value)
  {
      $attribute_name = "image";
      // or use your own disk, defined in config/filesystems.php
      $disk = config('backpack.base.root_disk_name');
      // destination path relative to the disk above
      $destination_path = "public/uploads/images/driverProfile";

      // if the image was erased
      if ($value==null) {
          // delete the image from disk
          \Storage::disk($disk)->delete($this->{$attribute_name});

          // set null in the database column
          $this->attributes[$attribute_name] = null;
      }

      // if a base64 was sent, store it in the db
      if (Str::startsWith($value, 'data:image'))
      {
          // 0. Make the image
          $image = \Image::make($value)->encode('jpg', 90);

          // 1. Generate a filename.
          $filename = md5($value.time()).'.jpg';

          // 2. Store the image on disk.
          \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());

          // 3. Delete the previous image, if there was one.
          \Storage::disk($disk)->delete($this->{$attribute_name});

          // 4. Save the public path to the database
          // but first, remove "public/" from the path, since we're pointing to it
          // from the root folder; that way, what gets saved in the db
          // is the public URL (everything that comes after the domain name)
          $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
          $this->attributes[$attribute_name] = $public_destination_path.'/'.$filename;
      }
  }
}
