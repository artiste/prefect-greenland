@extends(backpack_view('blank'))

@section('content')
    @php
        $widgets['before_content'][] = [
                  'type'         => 'alert',
                  'class'        => 'cart-color',
                  'heading'      =>  __('Welcome To Perfect Greenland Dashboard'),
                  'content'      =>  __('Welcome To Perfect Greenland System') ,
                  'close_button' => false, // show close button or not
                  ];

        $totalCustomer  = \App\Models\Customer::all()->count();
        $totalSales = \App\Models\Sale::all()->count();

        $higest_resolved=\Illuminate\Support\Facades\DB::select(\Illuminate\Support\Facades\DB::raw('SELECT product_id,SUM(qty)
        as resolved_total FROM order_items GROUP by product_id ORDER by resolved_total DESC'));

        if(count($higest_resolved) > 0){
            $topProductModel = \App\Models\Product::find($higest_resolved[0]->product_id);
            $topProduct = $topProductModel->name;
        } else{
            $topProduct = "---";
        }
    @endphp
    <style>
        .card-bg {
            background-color: #42ba96;
        }
    </style>
    <!-- admin modal --></br>
    <div class="tabbable-panel" style="padding: 15px;">
        <div class="tab-content">
            <label style="background-color: #42ba96; padding: 12px; text-align: center; color: white;"
                   class="col-md-12">Reports</label>
            <br>
            <div class="row justify-content-center">
                <div class="container-fluid">
                    <div class="card-culomns"  style='width: 100%;text-align:center;'>
                        <div class="d-flex flex-row justify-content-center align-items-center" style='margin:0px auto;' >
                            <div class="col-md-3">
                                <div class="card text-center text-white card-bg">
                                    <div class="card-body">
                                        <h5 class="card-title">Customers</h5>
                                        <p class="card-text" id="customers"><i
                                                style="color:red">{!! $totalCustomer !!}</i></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card text-center text-white card-bg">
                                    <div class="card-body">
                                        <h5 class="card-title">Sales</h5>
                                        <p class="card-text" id="sales"><i style="color:red">{!! $totalSales !!}</i></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card text-center text-white card-bg">
                                    <div class="card-body">
                                        <h5 class="card-title">Top Product</h5>
                                        <p class="card-text" id="topSales"><i style="color:red">{!! $topProduct !!}</i>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endsection

        @push('after_scripts')
            <script>
                $(document).ready(function () {
                    getReports();
                });

                function getReports() {
                    $.ajax({
                        'async': false,
                        url: "{{route('reports') }}",
                        method: 'GET',
                        data: {},
                        success: function (result) {
                            console.log(result);
                            document.getElementById('sales').innerHTML = 'RM ' + result['totalSales'];
                            document.getElementById('customers').innerHTML = result['customers'];
                            document.getElementById('topSales').innerHTML = result['topSales'][0]['name'] + ' (' + result['topSales'][0]['quantity'] + ')';
                            document.getElementById('orders').innerHTML = result['orders'];
                            document.getElementById('products').innerHTML = result['products'][4]['total'];
                        }
                    });
                }
            </script>
    @endpush

