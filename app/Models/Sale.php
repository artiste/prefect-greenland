<?php

namespace App\Models;

use App\Constant;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Sale extends Model implements HasMedia
{
    use HasFactory;
    use CrudTrait;
    use InteractsWithMedia;

    const STATUS_NEW = 'new';
    const STATUS_ON_HOLD = 'on-hold';
    const STATUS_PROCESSING = 'processing';
    const STATUS_CURRENT_DELIVERY = 'In Delivery';
    const STATUS_COMPLETED = 'completed';
    const STATUS_SHIPPING = 'shipped';
    const STATUS_REFUNDED = 'refunded';
    const STATUS_CANCELED = 'canceled';
    const STATUS = ['on-hold', 'processing', 'completed', 'shipped', 'refunded'];

    const IMAGE_PIC = "Pic Received";

    protected $fillable = [
        'id',
        'customer_id',
        'assignee',
        'wordpress_order_id',
        'order_key',
        'invoice_number',
        'status',
        'currency',
        'discount_total',
        'shipping_total',
        'subtotal',
        'total',
        'wordpress_customer_id',
        'customer_ip_address',
        'customer_note',
        'payment_method',
        'transaction_id',
        'date_paid',
        'date_completed',
        'delivery_date',
    ];

    /*
   |--------------------------------------------------------------------------
   | RELATIONS
   |--------------------------------------------------------------------------
   */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function driver()
    {
        return $this->belongsTo(User::class, 'assignee');
    }

    public function shippingAddress()
    {
        $check = $this->hasOne(Shipping::class, 'order_id', 'id');
        return $check;
    }

        public function remark()
    {
        return $this->hasMany(SaleOrderRemark::class, 'order_id');
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class, 'sale_id', 'id');
    }

    public function saveImage(Request $request)
    {
        if($request->has('image') && $request['image']!=null){
            $this->addMediaFromRequest('image')->toMediaCollection(Sale::IMAGE_PIC);
        }
    }

    public function getImageURL()
    {
        $result = collect();
        $reportImages = $this->getMedia(Sale::IMAGE_PIC);

        foreach ($reportImages as $image) {
            $result->push($image->getFullUrl());
        }

        return $result;
    }
}
