<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MetaData extends Model
{
    use HasFactory;

    CONST TYPE_ORDER = 'Order';
    CONST TYPE_ITEM = 'Item';

    protected $fillable =[
        'id',
        'foreign_id',
        'type',
        'metadata_id',
        'key',
        'value',
    ];
}
