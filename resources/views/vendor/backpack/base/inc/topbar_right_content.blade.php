<!-- This file is used to store topbar (right) items -->


{{-- <li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="la la-bell"></i><span class="badge badge-pill badge-danger">5</span></a></li>
<li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="la la-list"></i></a></li>
<li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="la la-map"></i></a></li> --}}
<ul class="nav navbar-nav ml-auto @if(config('backpack.base.html_direction') == 'rtl') mr-0 @endif">
    @php $locale = session()->get('locale'); @endphp
    <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            @switch($locale)
                @case('en')
                <img src="{{asset('images/en.png')}}"> English
                @break
                @case('ch')
                <img src="{{asset('images/ch.png')}}"> Chinese
                @break
               {{-- @case('ms')
                <img src="{{asset('images/ms.png')}}"> Malay
                @break--}}
                @default
                English
            @endswitch
            <span class="caret"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="{{ route('setLocale', 'en') }}"><img src="{{asset('images/en.png')}}">
                English</a>
            <a class="dropdown-item" href="{{ route('setLocale', 'ch') }}"><img src="{{asset('images/ch.png')}}">
                Chinese</a>
        </div>
    </li>
</ul>