<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;
use Hash;
use Illuminate\Support\Facades\Redirect;

//wooCommerce models and classes
use Codexshaper\WooCommerce\Models\Customer;
use Codexshaper\WooCommerce\Facades\WooCommerce;

class SyncUserController extends Controller
{

    /**
     * Display page for syncing users
     */
    public function index()
    {
        if(backpack_user()->role == User::ROLE_ADMIN){
            $customer = Customer::find(backpack_user()->wordpress_user_id);
            return view('admin.syncUser')->with('username', $customer['username']);
        }else{
            abort(404);
        }

    }

    /**
     * Get all wordpress customers
     */
    public function syncUsers(Request $request)
    {
            //Setup user name and password
            $username = $request['username'];
            $password = $request['password'];
            $wp_url = config('woocommerce.store_url');
            if(Hash::check($request['password'], backpack_user()->password)){
                // the standard end point for posts in an initialised Curl
                $process = curl_init($wp_url.'wp-json/wp/v2/users');

                // create the options starting with basic authentication
                curl_setopt($process, CURLOPT_USERPWD, $username . ":" . $password);
                curl_setopt($process, CURLOPT_TIMEOUT, 30);
                curl_setopt($process, CURLOPT_POST, 1);
                // make sure we are POSTing
                curl_setopt($process, CURLOPT_CUSTOMREQUEST, "GET");
                // allow us to use the returned data from the request
                curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);

                // process the request
                $return = curl_exec($process);
                curl_close($process);

                $result = json_decode($return, true);
                $users = $result;
                
                $wordpressusers = collect();
                foreach ($users as $user){
                    $existingUser = User::where('wordpress_user_id', $user['id'])->first();
                    if(!$existingUser){
                        $customer = Customer::find($user['id']);
                        User::create([
                            'name'=>$customer['first_name'],
                            'username'=>$customer['username'],
                            'password'=>'',
                            'email'=>$customer['email'],
                            'created_at'=>$customer['date_created'],
                            'wordpress_user_id'=>$customer['id']
                        ]);
                        $wordpressusers->add($customer);
                    }
                }
                return view('admin.unsyncusers')->with('users', $wordpressusers);
            }else{
                return Redirect::back()->withErrors(['Please check your password']);
            }
    }
}
