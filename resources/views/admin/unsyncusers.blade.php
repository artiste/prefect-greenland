@extends(backpack_view('blank'))
@section('header')
    <section class="container-fluid">
        <h2 class="col-md-8 text-center">
            <span class="text-capitalize">Sync Users</span>
        </h2>
    
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body">
                <table id="tablIed" class="table" width="100%">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                            <th>email</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user['first_name']}}</td>
                        <td>{{$user['last_name']}}</td>
                        <td>{{$user['username']}}</td>
                        <td>{{$user['email']}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="col-md-12 form-group">
                  <a href="{{ url('admin/dashboard') }}" class="btn btn-primary">Home</a>
                <div>
        </div>
      </div>
    </div>
</div>
@endsection


