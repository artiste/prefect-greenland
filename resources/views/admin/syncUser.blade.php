@extends(backpack_view('blank'))
@section('header')
    <section class="container-fluid">
        <h2 class="col-md-8 text-center">
            <span class="text-capitalize">Sync Users</span>
        </h2>
    
@endsection
@section('content')
<div class="row">
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <small class=" col-md-12 form-text">Please enter your password and submit to sync all the users that are not synced into admin panel<br></small>
          <form action="{{route('syncUsers')}}" method="POST">
            @csrf
            <div class="col-md-12 form-group">
              <label for="usename"><br>Username</label>
              <input type="email" name="username" class="form-control" value="{{$username}}" readonly>
            </div>
            <div class="form-group col-md-12">
              <label for="password">Password</label>
              <input type="password" name="password" class="form-control" required>
              @if($errors->any())
              <h6 style="color:red;">{{$errors->first()}}</h6>
              @endif
            </div>
            <div class="col-md-12 form-group">
              <button type="submit" class="btn btn-primary">Sync</button>
            <div>
          </form>
        </div>
      </div>
    </div>
</div>
@endsection


