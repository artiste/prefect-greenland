@php
    $products = \App\Models\Product::toBase()->get();

    if($crud->entry!=null){
        $orderItems = $entry->items;
    }else{
        $orderItems = null;
    }
@endphp

<div class="modal fade" id="invoiceItemModal" tabindex="0" role="dialog"
     aria-labelledby="orderitems-inline-create-dialog-label" aria-hidden="true">
    {{--<div class="modal fade" id="invoiceItemModal" tabindex="0" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header  bg-success">
                <h5 class="modal-title text-center">Add Order Items</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group required">
                    <label for="product_id"><b>Product Name</b></label>
                    <select type="text" class="form-control" name="product_id" id="product_id"
                            onchange="productChange()">
                        <option value="">-</option>
                        @foreach($products as $stock)
                            <option value="{{$stock->id}}">{{$stock->name}}</option>
                        @endforeach
                    </select>
                    <i class="text-danger" id="product_id_error"></i>
                </div>
                <div class="form-group required">
                    <label for="product_quantity"><b>Quantity (KG)</b></label>
                    <input type="number" class="form-control" name="product_quantity" id="product_quantity" min="1"
                           value="1" readonly step="3"
                           onchange="calculateSubTotal()" onkeyup="calculateSubTotal()">
                    <input type="hidden" class="form-control" name="product_measure" id="product_measure"
                           value="Unit">
                    <input type="hidden" class="form-control" name="available_stock" id="available_stock" value="1">
                    <i class="text-danger" id="product_quantity_error"></i>
                </div>
                <div class="form-group required">
                    <label for="product_price"><b>Price</b></label>
                    <input disabled type="text" class="form-control" name="product_price" id="product_price">
                    <input type="hidden" class="form-control" name="product_name" id="product_name">
                </div>
                <div class="form-group required">
                    <label for="product_subtotal"><b>Subtotal</b></label>
                    <input disabled type="text" step="any" class="form-control" name="product_subtotal"
                           id="product_subtotal">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="save_invoice_item" onclick="saveButton()">Save
                </button>
            </div>
        </div>
    </div>
</div>


<!-- text input -->
@include('crud::fields.inc.wrapper_start')
<label>{!! $field['label'] !!}</label>
@include('crud::fields.inc.translatable_icon')

<input class="array-json"
       type="hidden"
       data-init-function="bpFieldInitTableElement"
       name="{{ $field['name'] }}"
       value="">


<div class="col-md-12">
    <div class="form-group required">
        <div class="table-responsive">
            <table id="items_table" name='items_table' class="table table-striped table-bordered"
                   style="width:100%">
                <thead>
                <tr>
                    <th style="width:10%;">#</th>
                    <th>Product</th>
                    <th>Price / Unit</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                    <th style="display:none;">@lang('lang.Product ID')</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($orderItems))
                    @for ($i = 0; $i < count($orderItems); $i++)
                        @php
                            $item = $orderItems[$i];
                            $product = $item->product;
                        @endphp
                        <tr>
                            <td>{!! $i+1 !!}</td>
                            <td>{!! $item->name  !!}</td>
                            <td>{!! $product->price !!}</td>
                            <td>{!! $item->qty !!}</td>
                            <td>{!! $item->total !!}
                                <button class="btn btn-danger remove-button" type="button"
                                        onclick="removeCartItem(event)">
                                    <i class="la la-trash"></i>
                                </button>
                            </td>
                            <td style="display:none;" class="product_id" alt='{!! $item->id !!}'>
                            <label data-alt="${selectedInventory['id']}" class="product-image" >{!! $item->id !!}</label>
                            </td>
                        </tr>
                    @endfor
                @endif
                </tbody>
            </table>
            <button type="button" id="add_product" class="btn btn-primary"
                    onclick="openInvoiceItemModal()">Add
            </button>
        </div>
    </div>
</div>


{{-- HINT --}}
@if (isset($field['hint']))
    <p class="help-block">{!! $field['hint'] !!}</p>
@endif

@include('crud::fields.inc.wrapper_end')

<script>
    let productList = <?php echo json_encode($products); ?>;
    let selectedInventory;
    let listItem = [];
    let itemIndex = 0;

    function openInvoiceItemModal() {
        $('#invoiceItemModal').appendTo('body').modal('show');
    }

    function productChange() {
        var product_id = $("#product_id").val();
        selectedInventory = productList.find(item => item['id'] == product_id)
        if (selectedInventory != null) {
            $("#product_quantity").removeAttr('readonly');
        }
        if (parseFloat(selectedInventory['discount_price']) > 0) {
            $('#product_price').val(selectedInventory['discount_price'])
        } else {
            $('#product_price').val(selectedInventory['price'])
        }
        calculateSubTotal()
    }

    function calculateSubTotal() {
        var quantity = $("#product_quantity").val()
        var unitPrice = 0
        if (parseFloat(selectedInventory['discount_price']) > 0) {
            unitPrice = selectedInventory['discount_price']
        } else {
            unitPrice = selectedInventory['price']
        }
        var subTotal = parseFloat(quantity).toFixed(3) * parseFloat(unitPrice)

        $("#product_subtotal").val(parseFloat(Math.round(subTotal * 100) / 100).toFixed(2))
    }

    function saveButton() {
        if (selectedInventory == null) {
            alert("Please select product.")
        } else {
            var quantity = $("#product_quantity").val();
            var unitPrice = $("#product_price").val();
            var subTotal = $("#product_subtotal").val();

            let cloneItem = {};
            cloneItem = listItem.find(item => {
                if (
                    item['id'] == selectedInventory['id']) return item
            })

            if (cloneItem !== undefined) {
                cloneItem['total_quantity'] = parseFloat(cloneItem['total_quantity']) + parseFloat(quantity)
                var _amount = parseFloat(cloneItem['total_amount']) + parseFloat(subTotal)
                cloneItem['total_amount'] = _amount.toFixed(2)
                listItem = listItem.map(item => {
                    if (item['index'] == cloneItem['index']) {
                        return cloneItem
                    }
                    return item
                })

                // const currentItem = $(`[alt=${cloneItem['index']}]`).parents()[3];
                // $(currentItem).find('td .product-quantity').text(cloneItem['quantity'])
                // $(currentItem).find('td .product-amount').text(parseFloat(cloneItem['amount']).toFixed(2))
            } else {
                selectedInventory['index'] = itemIndex
                selectedInventory['total_quantity'] = quantity
                selectedInventory['unitPrice'] = unitPrice
                selectedInventory['total_amount'] = subTotal

                console.log(selectedInventory);


                listItem = [...listItem, selectedInventory]

                var orderItems = `

                        <td>${selectedInventory['index'] + 1}</td>
                        <td>${selectedInventory['name']}</td>
                        <td>${selectedInventory['unitPrice']}</td>
                        <td>${selectedInventory['total_quantity']}</td>
                        <td>${selectedInventory['total_amount']}<button class="btn btn-danger remove-button" type="button" onclick="removeCartItem(event)">
                                <i class="la la-trash" ></i>
                            </button>
                        </td>
                        <td style="display:none;" class="product_id" alt='${selectedInventory['id']}'>
                            <label data-alt="${selectedInventory['id']}" class="product-image" >${selectedInventory['id']} </label>
                        </td>
                        `
                var cartRow = document.createElement('tr')
                cartRow.classList.add('cart-row')
                var cartItems = document.getElementById('items_table')
                cartRow.innerHTML = orderItems
                cartItems.append(cartRow)
            }

            itemIndex = itemIndex + 1

            $('#invoiceItemModal').modal('hide');
            updateSubTotal();
            return
        }
    }

    function updateSubTotal() {
        var total = 0;
        for (var i = 0; i < listItem.length; i++) {
            total += parseFloat(listItem[i]['total_amount']).toFixed(2) << 0;
        }

        $('input[name="subtotal"]').val(total.toFixed(2));
        updateTotalAmount(parseFloat(total));

        $('input[name="order_items"]').val(JSON.stringify(listItem));

    }


    const removeCartItem = async (event) => {
        var buttonClicked = event.target
        const itemInfo = buttonClicked.parentElement.parentElement.parentElement
        var productIndex = parseInt(itemInfo.getElementsByClassName('product-image')[0].getAttribute('data-alt'))
        console.log(productIndex);

        listItem = $.grep(listItem, function (e) {
            return e.id != productIndex;
        });

        itemInfo.remove()
        itemIndex--
        updateSubTotal()
    }

</script>


