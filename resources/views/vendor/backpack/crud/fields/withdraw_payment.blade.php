@php
$payments = \App\Models\WithdrawalPayment::where('withdrawal_id', $crud->entry->id)->get();
@endphp
@include('crud::fields.inc.wrapper_start')
@if(backpack_user()->role!=App\Models\User::ROLE_MEMBER && $crud->entry->status != App\Models\Withdraw::STATUS['cancel'])
<button class="btn btn-primary" style="margin-top: 10px; float:right;" id="save_payment" value="save_payment" name="save_payment" onclick="save_payment()">Save Payment</button><br><br>
<hr style="height:5px;border-width:0;color:gray;background-color:#161c2d!important"></hr>
@endif
<table class="table" width="100%">
    <thead>
        <tr>
            <th>Paid Date</th>
            <th>Amount</th>
            <th>Payment Type</th>
            <th>Paid by</th>
            <th>Transaction ID</th>
            <th>Remark</th>
            <th>Receipt</th>
            <th>Action</th>
        <tr>
    </thead>
    <tbody>
        @foreach ($payments as $payment)
        <tr>
            <td>{{\Carbon\Carbon::parse($payment->paid_date)->format('d/m/Y H:i:s')}}</td>
            <td>{{$payment->amount}}</td>
            <td>
            @switch($payment->type)
                @case(0)
                    Bank Transfer
                    @break
                @case(1)
                    Cash
                    @break
                @case(2)
                    Cheque
                    @break
                @case(3)
                    Terawallet
                    @break
                @default
                    {{$payment->type}}
                    @break
            @endswitch
            </td>
            <td>{{$payment->user->name}}</td>
            <td>{{$payment->transaction_id}}</td>
            <td>{{$payment->remark}}</td>
            <td>{{$payment->reciept}}</td>
            <td><a type="button" href="#" onclick="deletePayment({{$payment->id}})">delete</a></td>
        <tr>
        @endforeach
    </tbody>
</table>
<script>
    function deletePayment(id) {
        swal({
            title: "{!! trans('backpack::base.warning') !!}",
            text: "Are you sure you want to delete this payment item!",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "Cancel",
                    value: null,
                    visible: true,
                    className: "bg-secondary",
                    closeModal: true,
                },
                delete: {
                    text: "Delete",
                    value: true,
                    visible: true,
                    className: "bg-danger",
                }
            },
        }).then((value) => {
            if (value) {
                $.ajax({
                    url: '{{route('deletePayment')}}',
                    type: 'POST',
                    data: {
                        'id': id
                    },
                    success: function (result) {
                        console.log(result);
                        if (result == 'saved') {
                            // Show a success notification bubble
                            new Noty({
                                type: "success",
                                text: "<strong>Payment Deleted</strong><br> This payment is successfully deleted"
                            }).show();
                            // Hide the modal, if any
                            $('.modal').modal('hide');
                                setTimeout(function (){
                                    location.reload();
                                }, 1000);
                        }else {// Show an error alert
                                swal({
                                    title: "<?php echo trans('backpack::crud.delete_confirmation_not_title'); ?>",
                                    text: "There's been an error. The payment might not have been deleted.",
                                    icon: "error",
                                    timer: 4000,
                                    buttons: false,
                                });
                        }
                    },
                    error: function (result) {
                        // Show an alert with the result
                        swal({
                            title: "<?php echo trans('backpack::crud.delete_confirmation_not_title'); ?>",
                            text: "There's been an error. The payment might not have been removed.",
                            icon: "error",
                            timer: 4000,
                            buttons: false,
                        });
                    }
                });
            
             }
        });

    }
</script>
