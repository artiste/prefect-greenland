<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/**Route for login API */
Route::post('login', 'Api\ApiController@login')->name('login');

/**Route for register API */
Route::post('register', 'Api\ApiController@register')->name('register');

Route::get('testing', 'WoocommerceController@orderComplete')->name('testing');

Route::post('/syncUser','WoocommerceController@syncUser');
Route::post('/orderComplete','WoocommerceController@orderComplete');

/**Route for details user API */
Route::middleware('auth:api')->group(function(){
    Route::get('driverTaskList', 'Api\DriverApiController@driverTaskList');
    Route::get('driverTaskDetail/{taskId}', 'Api\DriverApiController@driverTaskDetail');
    Route::post('syncLocation', 'Api\DriverApiController@syncLocation');
    Route::post('updateOrder/{taskId}', 'Api\DriverApiController@updateOrder');
    Route::post('syncUserAppToken', 'Api\ApiController@syncUserAppToken')->name('syncUserAppToken');
    Route::post('updateDriverProfile', 'Api\DriverApiController@updateProfile')->name('updateProfile');

    Route::get('me','Api\DriverApiController@getDriverProfile');
});
