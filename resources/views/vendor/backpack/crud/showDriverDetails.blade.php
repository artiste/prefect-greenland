@extends(backpack_view('blank'))

@php
  $defaultBreadcrumbs = [
    trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
    $crud->entity_name_plural => url($crud->route),
    trans('backpack::crud.preview') => false,
  ];

  // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
  $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
  $lastloCation = $entry->location->sortByDesc('id')->first();
  if($lastloCation){
	$currentPosition = $lastloCation;
	$lat = $currentPosition->lat;
	$lng = $currentPosition->lng;
  }else{
	$currentPosition = '';
	$lat = '';
	$lng = '';
  }
  $now = \Carbon\Carbon::now(); // or whatever you're using to set it
  $startOfDay = $now->copy()->startOfDay();
  $location = $entry->location->whereBetween('created_at', array($startOfDay, $now));
  $positions = '';
  if($location){
	foreach($location as $position){
		$positions .= $position->lat.' '.$position->lng.',';
	}
  }
@endphp

@section('header')
	<section class="container-fluid d-print-none">
    	<a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
		<h2>
	        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
	        <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name !!}.</small>
	        @if ($crud->hasAccess('list'))
	          <small class=""><a href="{{ url($crud->route) }}" class="font-sm"><i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
	        @endif
	    </h2>
    </section>
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">

	<!-- Default box -->
	  <div class="">
	  	@if ($crud->model->translationEnabled())
	    <div class="row">
	    	<div class="col-md-12 mb-2">
				<!-- Change translation button group -->
				<div class="btn-group float-right">
				  <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    {{trans('backpack::crud.language')}}: {{ $crud->model->getAvailableLocales()[request()->input('locale')?request()->input('locale'):App::getLocale()] }} &nbsp; <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				  	@foreach ($crud->model->getAvailableLocales() as $key => $locale)
					  	<a class="dropdown-item" href="{{ url($crud->route.'/'.$entry->getKey().'/show') }}?locale={{ $key }}">{{ $locale }}</a>
				  	@endforeach
				  </ul>
				</div>
			</div>
	    </div>
	    @else
	    @endif
	    <div class="card no-padding no-border">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-selected="true">Details</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="location-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">GPS Location</a>
				</li>
			</ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" style="background:white;" id="details" role="tabpanel" aria-labelledby="details-tab">
					<div class="row">
						<div class="col-md-5">
							<table class=" table table-striped mb-0">
								<tbody>
								@foreach ($crud->columns() as $column)
									<tr>
										<td>
											<strong>{!! $column['label'] !!}:</strong>
										</td>
										<td>
											@if (!isset($column['type']))
											@include('crud::columns.text')
											@else
											@if(view()->exists('vendor.backpack.crud.columns.'.$column['type']))
												@include('vendor.backpack.crud.columns.'.$column['type'])
											@else
												@if(view()->exists('crud::columns.'.$column['type']))
												@include('crud::columns.'.$column['type'])
												@else
												@include('crud::columns.text')
												@endif
											@endif
											@endif
										</td>
									</tr>
								@endforeach
								@if ($crud->buttons()->where('stack', 'line')->count())
									<tr>
										<td><strong>{{ trans('backpack::crud.actions') }}</strong></td>
										<td>
											@include('crud::inc.button_stack', ['stack' => 'line'])
										</td>
									</tr>
								@endif
								</tbody>
							</table>
						</div>
						<div class="col-md-7">
							<div  id="map" style="width:100%; height:430px;"></div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="location" style="background:white;" role="tabpanel" aria-labelledby="location-tab">
					<div class="row">	
						<div class="col-md-12">
							<div id="map_canvas" style="width:100%; height:430px;"></div>
						</div>
					</div>
				</div>
			</div>
	    </div><!-- /.box-body -->
	  </div><!-- /.box -->

	</div>
</div>
@endsection


@section('after_styles')
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css') }}">
@endsection

@section('after_scripts')
	<script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
	<script src="{{ asset('packages/backpack/crud/js/show.js') }}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyBF3mASZlrh2H-nygEJmJ_5lrAGsFoKL3E&callback=initialize"></script>
	<script>

	var geocoder;
	var map;
	var bounds = new google.maps.LatLngBounds();
	function initialize() {
		var lat = "{{$lat}}";
		var lng = "{{$lng}}";
		var positions = "{{$positions}}";
		if(lat != '' && lng != ''){
			var lastLocation = {lat: parseFloat(lat), lng: parseFloat(lng)};	
			currenTmap = new google.maps.Map(
			document.getElementById("map"), {
				center: new google.maps.LatLng(parseFloat(lat), parseFloat(lng)),
				zoom: 14,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			var marker = new google.maps.Marker({
				position: lastLocation,
				map: currenTmap,
			});	
		}else{
			document.getElementById("map").innerHTML = "Location is not available";
		}
		if(lat != '' && lng != ''){
			positions = positions.split(',');
			todayMap = new google.maps.Map(
			document.getElementById("map_canvas"), {
				center: new google.maps.LatLng(lat, lng),
				zoom: 13,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			for(var i = 0; i<positions.length-1; i++){
				var position = positions[i].split(" ");
				var location = { lat: parseFloat(position[0]), lng: parseFloat(position[1]) };
				console.log(location);
				var marker = new google.maps.Marker({
					position: location,
					map: todayMap,
				});
			}
		}else{
			document.getElementById("map_canvas").innerHTML = "Location is not available";
		}
	}
	google.maps.event.addDomListener(window, "load", initialize);
</script>
@endsection
